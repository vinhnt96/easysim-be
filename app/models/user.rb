class User < ApplicationRecord
  rolify
  extend Devise::Models
  devise :database_authenticatable,
         :invitable,
         :registerable,
         :recoverable,
         :rememberable,
         :validatable,
         :trackable,
         :confirmable

  include DeviseTokenAuth::Concerns::User

  has_many :user_strategy_selections, dependent: :destroy
  has_many :strategy_selections, through: :user_strategy_selections
  has_many :subscriptions, dependent: :destroy
  has_many :ip_histories
  has_many :bug_reports
  has_many :activities
  has_one :user_preference, dependent: :destroy
  has_one :universal_note, dependent: :destroy

  scope :confirmed,  -> { where("confirmed_at IS NOT NULL") }
  scope :un_confirmed,  -> { where("confirmed_at IS NULL") }

  validates :api_key, presence: true, uniqueness: true
  validates :provider, uniqueness: { scope: :uid }, if: -> {uid.present? }
  validates :user_type, presence: true, inclusion: { in: ['free', 'paid', 'promotional'] }

  before_validation :generate_api_key, on: :create
  after_create :create_preference, :assign_default_role, :create_universal_note
  before_invitation_created :generate_api_key

  def token_validation_response
    UserSerializer.new( self, root: false).as_json
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def free_trial?
     user_type == :free || user_type == "free"
  end

  def role
    roles.first
  end

  def full_access?
    access || role.admin?
  end

  def current_activity
    activities
      .where(signout_time: nil)
      .first
  end

  def current_subscription
    Subscription.find_by(id: current_subscription_id)
  end

  def accessable_game_type
    return 'cash-mtts' if self.has_role?('admin')
    return '' if user_type.to_sym == :free && accessibility_game_type.nil?    # accessibility_game_type will used for giving accessibility without subscription (mtt OR cash)
    current_subscription&.plan&.parent_code if current_subscription&.not_ended?
  end

  def admin_access
    return 'cash-mtts' if access
    accessibility_game_type
  end

  def access
    (user_type.to_sym == :promotional && accessibility_game_type.nil?) || accessibility_game_type === 'cash-mtts'
  end

  def not_access
    (accessibility_game_type === 'free'|| accessibility_game_type.nil?) && user_type.to_sym == :free
  end

  private

  def create_preference
    UserPreference.create(user_id: id)
  end

  def assign_default_role
    if self.roles.blank?
      if current_subscription
        self.add_role(:odin_user)
      else
        self.add_role(:free_user)
      end
    end
  end

  def generate_api_key
    Retryable.retryable(tries: 5) do
      self.api_key = SecureRandom.alphanumeric(6)
      raise "Api has already been taken" if self.errors.full_messages.include? ('Api has already been taken')
    end
  end

  def create_universal_note
    UniversalNote.create(user_id: id, note_data: { default_note: "", notes: [] }.to_json)
  end

  def send_devise_notification(notification, *args)
    message = devise_mailer.send(notification, self, *args)

    # Deliver later with Active Job's `deliver_later`
    if message.respond_to?(:deliver_later)
      message.deliver_later
    # Remove once we move to Rails 4.2+ only, as `deliver` is deprecated.
    elsif message.respond_to?(:deliver_now)
      message.deliver_now
    else
      message.deliver
    end
  end
end
