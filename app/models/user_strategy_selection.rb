class UserStrategySelection < ApplicationRecord
  belongs_to :user
  belongs_to :strategy_selection

  serialize :notes
  validates :user_id, uniqueness: { scope: :strategy_selection_id }
  validates :user_id, :strategy_selection_id, presence: true
    
  scope :free_trial, -> { joins(:strategy_selection).where(strategy_selections: { strategy_selection_type: 'free' }) }
end
