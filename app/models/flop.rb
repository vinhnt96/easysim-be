class Flop < ApplicationRecord

  def next_flops
    Flop.where(
      game_type: game_type,
      specs: specs,
      stack_size: stack_size,
      sim_type: sim_type,
      positions: positions,
      flop_cards: flop_cards,
      node_count: node_count+1,
    ).where("nodes LIKE ?", "#{nodes}%")
  end
end
