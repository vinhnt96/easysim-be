class StrategySelection < ApplicationRecord
  has_many :user_strategy_selections
  has_many :users, through: :user_strategy_selections

  validates :game_type, :stack, :street, :positions, :flops, presence: true
end
