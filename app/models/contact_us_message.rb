class ContactUsMessage < ApplicationRecord
  after_create_commit :send_contact_message_email

  validates :email, :message, :name, presence: true

  def send_contact_message_email
    Mailing::ContactUsMessageWorker.perform_async(self.id)
  end
end
