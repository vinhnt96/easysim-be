class BugReport < ApplicationRecord
  belongs_to :user

  after_create_commit :send_bug_report_email

  validates :bug_type, :subject, :description, presence: true

  def send_bug_report_email
    Mailing::BugReportWorker.perform_async(self.id)
  end
end
