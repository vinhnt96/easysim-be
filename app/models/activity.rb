class Activity < ApplicationRecord
  belongs_to :user
  validates :logged_time, :user_id, presence: true

  after_update :send_warning_activities_limit

  def sim_ids_in_date
    user
      .activities
      .where(begin_date: Time.current.to_date)
      .pluck(:sim_ids)
  end

  def total_sim_ids
    sim_ids_in_date
      .flatten
      .uniq
      .length
  end

  def send_warning_activities_limit
    if total_sim_ids > LIMIT_SIMS
      Mailing::WarningActivitytWorker.perform_async(user.email)
    end
  end
end
