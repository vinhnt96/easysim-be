class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  connects_to shards: {
    default: { writing: :master, reading: :master },
    flops: { writing: :flops, reading: :flops }
  }
end
