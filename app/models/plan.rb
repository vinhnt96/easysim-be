class Plan < ApplicationRecord

  validates :product_id, :name, :price, :currency, presence: true
  validates :product_id, uniqueness: true

  has_many :subscriptions

end
