class IpHistory < ApplicationRecord
  belongs_to :user
  validates :ip, :user_id, presence: true
end