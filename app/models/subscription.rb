class Subscription < ApplicationRecord
  belongs_to :user
  belongs_to :plan

  validates :start_time, :plan_id, presence: true

  before_destroy :deactivate_subscription_on_FS, if: Proc.new { |subscription| subscription.not_ended? }

  enum status: [:active, :expired, :canceled, :deactivated, :failed]
  enum discount: {
    pokercode: 0.45,
    ggplatinum: 0.35,
    ginge: 0.35,
    elliot: 0.35,
    bts: 0.35,
    streamteam: 0.35,
    cavalitopoker: 0.35,
    pc: 0.35,
    kmart: 0.35,
    standard: 0.35,
    pads: 0.35,
    pav: 0.35,
    jnandez: 0.35,
    pc50: 0.5,
    reglife: 0.35,
  }

  def deactivate_subscription_on_FS
    fastspring_auth = Fastspring::BaseService.get_credentials
    Subscriptions::Update.new(user, {fastspring_subscription_id: fastspring_subscription_id}, fastspring_auth).deactivate_subscription
  end

  def not_ended?
    self.active? || self.canceled?
  end
end
