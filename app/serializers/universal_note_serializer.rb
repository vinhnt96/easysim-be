class UniversalNoteSerializer < ActiveModel::Serializer
  attributes :note_data, :id
end