class UserPreferenceSerializer < ActiveModel::Serializer
  attributes :id, :fold_color, :check_and_call_color, :bet_and_raise_color, 
            :round_strategies_to_closest, :hide_strategies_less_than, :hide_explanation, :display_positions_by_default
end
