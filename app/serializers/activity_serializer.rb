class ActivitySerializer < ActiveModel::Serializer
  attributes :logged_time, :sim_ids, :signout_time, :total_time_spent_logged, :begin_date

  def total_time_spent_logged
    end_time = object.signout_time.present? ? object.signout_time : DateTime.current
    end_time.to_i - object.logged_time.to_i
  end
end
