class UserSerializer < ActiveModel::Serializer
  attributes :email, :strategy_selection, :id, :user_strategy_selection, :role, :first_name, :last_name, :accessable_game_type,
             :birthday, :country, :role_id, :created_at, :email_confirmed, :api_key, :user_type, :last_ip, :sim_ids, :current_ip_v4, :current_signin_ip_v4_at, :device, :browser, :online, :country_login, :affiliate_code
  attributes :preferences, :subscription, :universal_note, :full_access, :admin_access

  def strategy_selection
    if object.strategy_selection_id
      strategy_selection = StrategySelection.find_by(id: object.strategy_selection_id)
      StrategySelectionSerializer.new(strategy_selection).as_json if strategy_selection
    end
  end

  def user_strategy_selection
    if object.strategy_selection_id
      user_strategy_selection = UserStrategySelection.find_by(user_id: object.id, strategy_selection_id: object.strategy_selection_id)
      MySimSerializer.new(user_strategy_selection).as_json if user_strategy_selection.present?
    end
  end

  def accessable_game_type
    object.accessable_game_type
  end

  def preferences
    UserPreferenceSerializer.new(object.user_preference).as_json if object.user_preference
  end

  def last_ip
    object.last_sign_in_ip.to_s
  end

  def role
    object.roles&.first&.name
  end

  def role_id
    object.roles&.first&.id
  end

  def subscription
    SubscriptionSerializer.new(object.current_subscription).as_json unless object.current_subscription.blank?
  end

  def universal_note
    UniversalNoteSerializer.new(object.universal_note).as_json if object.universal_note
  end

  def full_access
    object.full_access?
  end

  def admin_access
    object.admin_access
  end

  class MySimSerializer < ActiveModel::Serializer
    attributes :notes, :id
  end

  def email_confirmed
    object.confirmed?
  end
end
