class FlopSerializer < ActiveModel::Serializer
  attributes %i(
    id
    nodes
    node_count
    pot_main
    pot_ip
    pot_oop
    strategy
    num_combos_oop
    num_combos_ip
    ev_oop
    ev_ip
    equity_ip
    equity_oop
    ranges_ip
    ranges_oop
  )
end
