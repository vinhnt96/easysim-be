class StrategySelectionSerializer < ActiveModel::Serializer
  attributes :game_type, :flop_cards, :stack_size, :sim_type, :positions, :street,
    :site, :stake, :open_size, :id, :specs
  def flop_cards
    object.flops
  end

  def stack_size
    object.stack
  end
end
