class SubscriberSerializer < ActiveModel::Serializer
  attributes :email
end
