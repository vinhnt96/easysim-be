class SubscriptionSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :code_discount, :paid, :status, :fastspring_subscription_id, :price_discount
  attributes :email, :active_subscription, :full_access, :user_id, :billing_cycle, :payment, :plan, :renew_time, :auto_renew, :accessable_game_type, :admin_access

  def email
    object.user&.email
  end

  def plan_subscription
    object.plan
  end

  def plan
    PlanSerializer.new(plan_subscription).as_json if plan_subscription.present?
  end

  def price_discount
    return 0 unless plan_subscription.present?
    discount_by_enum = Subscription.discounts[convert_code_discount_to_enum(object.code_discount)]
    discount_percent = object.code_discount == "LAUNCH35" ? 0.35 : discount_by_enum.present? ? discount_by_enum : 0.0
    plan_subscription.price * (1-discount_percent)
  end

  def active_subscription
    object.plan.name
  end

  def billing_cycle
   object.plan.billing_frequency == 1 ? convert_billing_period : "#{object.plan.billing_frequency} #{convert_billing_period}"
  end

  def payment
    "#{price_discount} #{object.plan.currency}"
  end

  def full_access
    object.user&.full_access?
  end

  def accessable_game_type
    object.user&.accessable_game_type
  end

  def admin_access
    object.user&.admin_access
  end

  private
  def convert_billing_period
    billing_period = object.plan.billing_period
    billing_period[0].upcase + billing_period.slice(1,billing_period.length).downcase + "ly"
  end

  def convert_code_discount_to_enum(code_discount)
    CODE_DISCOUNT_TO_ENUM_MAPPING[code_discount] if code_discount
    ""
  end
end
