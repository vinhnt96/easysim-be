class HandCategorySerializer < ActiveModel::Serializer
  attributes :board, :made_hands, :draw_hands
end