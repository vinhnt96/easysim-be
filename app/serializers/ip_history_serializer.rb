class IpHistorySerializer < ActiveModel::Serializer
  attributes :id, :ip_info, :created_at
  def ip_info
    object.ip.to_s
  end
end
