class CurrentFlopSerializer < FlopSerializer
  attribute :next_flops

  def next_flops
    ActiveModelSerializers::SerializableResource.new(object.next_flops, each_serializer: FlopSerializer).as_json
  end

end
