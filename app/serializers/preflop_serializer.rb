class PreflopSerializer < ActiveModel::Serializer
  attributes :game_type, :stack_size, :node
  attribute :action_options
  attribute :action_ranges

  def action_options
    action_arr = object.actions.split(',')

    action_arr.map do |action|
      { title: action, value: action.downcase}
    end
  end

  def action_ranges
    action_arr = object.actions.split(',')
    range_arr = object.ranges.split(',')
    action_ranges = {}

    action_arr.each_with_index do |action, index|
      action_ranges[action.downcase] = range_arr[index]
    end
    action_ranges
  end
end
