class RoleSerializer < ActiveModel::Serializer
  attributes :id, :name, :title, :resource_type, :resource_id
end
