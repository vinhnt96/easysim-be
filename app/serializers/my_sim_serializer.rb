class MySimSerializer < ActiveModel::Serializer
  attributes :notes, :date_last_view, :id, :active

  belongs_to :strategy_selection

end
