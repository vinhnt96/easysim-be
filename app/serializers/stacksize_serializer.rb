class StacksizeSerializer < ActiveModel::Serializer
  attributes :filename, :initial_stacksize
end
  