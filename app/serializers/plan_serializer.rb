class PlanSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :name, :price, :currency, :parent, :parent_code, :billing_frequency, :billing_period
end
