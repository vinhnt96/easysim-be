class ApplicationMailer < ActionMailer::Base
  default from: ENV['MAIL_DEFAULT_SENDER']
  layout 'mailer'
  include EmailHelper
  helper EmailHelper
end
