class DeviseMailer < Devise::Mailer
  layout 'mailer'
  include ActionView::Helpers::OutputSafetyHelper
  include EmailHelper
  helper EmailHelper

  def reset_password_instructions(record, token, opts={})
    I18n.locale = :en
    super
  end

  def confirmation_instructions(record, token, opts={})
    I18n.locale = :en
    super
  end

  def invitation_instructions(record, token, opts={})
    I18n.locale = :en
    @not_subscription = opts[:not_subscription] || false
    super
  end
end
