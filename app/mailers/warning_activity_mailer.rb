class WarningActivityMailer < ApplicationMailer
  def send_warning_activity_email(email)
    @email = email
    mail(
      from: email,
      to: ENV['MAIL_DEFAULT_SENDER'],
      subject: "Someone is viewing too many sims",
    )
  end
end
