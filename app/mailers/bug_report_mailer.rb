class BugReportMailer < ApplicationMailer
  def send_report_bug_email(report)
    @report = report
    @user = @report.user

    mail(
      from: @user.email,
      to: 'odinpokertestmail@gmail.com',
      subject: "New bug reported from #{@user.full_name}",
    )
  end
end
