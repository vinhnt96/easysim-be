class ContactUsMailer < ApplicationMailer
  def send_contact_message message
    @contact_message = message

    mail(
      from: @contact_message.email,
      to: 'odinpokertestmail@gmail.com',
      subject: "Contact Message from #{@contact_message.name}"
    )
  end
end
