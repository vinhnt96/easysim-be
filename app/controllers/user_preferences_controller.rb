class UserPreferencesController < ApplicationController
  before_action :set_preference, only: [:update]

  def update
    @user_preference.update(user_preference_params)

    render_api(
      data: @user_preference.user,
      status: :ok,
      serializer: UserSerializer,
    )
  end

  private

  def set_preference
    @user_preference = current_user.user_preference
  end

  def user_preference_params
    params.require(:user_preference).permit(
      :fold_color,
      :check_and_call_color,
      :bet_and_raise_color,
      :round_strategies_to_closest,
      :hide_strategies_less_than,
      :hide_explanation,
      :display_positions_by_default
    )
  end
end