class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: [:check_exists]

  def show
    render json: { user: user }
  end

  def update
    result = DeviseTokenAuthOverrides::Users::Update.new(user).excute(params_permit)
    render json: { data: result }
  end

  def update_info_current_login
    result = DeviseTokenAuthOverrides::Users::InfoCurrentLogin.new(user).excute(params_info_permit)
    render json: { user: result }
  end

  def check_exists
    exists_code = User.where(email: params[:email]).exists? ? 1 : 0
    render json: { exists_code: exists_code }
  end

  private

  def params_permit
    params.permit(:add, :sim_ids)
  end

  def params_info_permit
    params.permit(:device, :browser, :online, :current_ip_v4)
  end

end
