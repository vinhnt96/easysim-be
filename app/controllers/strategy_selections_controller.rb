class StrategySelectionsController < ApplicationController
  include HTTParty

  before_action :check_existing_channel
  before_action :check_valid_params, only: [:create]
  after_action :update_current_activity

  def create
    @strategy_selection = StrategySelectionsService.new(params, current_user, strategy_selection_params).create_strategy_selection

    if @strategy_selection
      @user_strategy_selection = @strategy_selection.user_strategy_selections.last
      render_api(
        data: {
          strategy_selection: StrategySelectionSerializer.new(@strategy_selection),
          user_strategy_selection: UserSerializer::MySimSerializer.new(@strategy_selection.user_strategy_selections.last),
        },
        status: :created
      )
    else
      render json: @strategy_selection.errors, status: :unprocessable_entity
    end    
  end

  def get_strategy_selection
    @strategy_selection = StrategySelectionsService.new(params, current_user, {}, user_strategy_selection).get_strategy_selection
    render_api(
      data: @strategy_selection,
      status: :ok,
      serializer: StrategySelectionSerializer
    )
  end

  private

    def strategy_selection_params
      params.require(:strategy_selection).permit(
        :game_type, :specs, :stack, :sim_type, :street, :site, :stake, :open_size, positions: [], flops: [])
    end

    def user_strategy_selection
      @user_strategy_selection ||= current_user.user_strategy_selections.find(params[:user_strategy_selection_id])
    end

    def update_current_activity
      return if !activity.present?
      new_sim_ids = activity.sim_ids
      unless new_sim_ids.include? user_strategy_selection.id
        new_sim_ids << user_strategy_selection.id
        activity.update(sim_ids: new_sim_ids)
      end
    end

    def activity
      current_user.current_activity
    end

    def check_valid_params
      strategy_selection_obj_dup = strategy_selection_params.to_h
      return true if strategy_selection_params[:street] == 'preflop' || current_user.full_access? || [SIX_MAX_CASH_GAME_STRATEGY, MTT_GAME_STRATEGY, HUNL_CASH_GAME_STRATEGY].include?(strategy_selection_obj_dup)
      raise APIErrors::UnprocessableEntity.new(error_message: "You can not access the game.") if current_user.not_access

      accessable_game_type = current_user.accessable_game_type
      accessibility_game_type = current_user.accessibility_game_type
      admin_access = current_user.admin_access

      if accessibility_game_type.nil? || accessibility_game_type === 'free'
        if accessable_game_type == 'cash'
          raise APIErrors::UnprocessableEntity.new(error_message: "You can only access CASH game.") if strategy_selection_params[:game_type] != 'cash'
        elsif accessable_game_type == 'mtts'
          raise APIErrors::UnprocessableEntity.new(error_message: "You can only access MTT game.") if strategy_selection_params[:game_type] != 'mtt'
        end
      else
        if admin_access === 'cash'
          raise APIErrors::UnprocessableEntity.new(error_message: "You can only access CASH game.") if strategy_selection_params[:game_type] != 'cash'
        elsif admin_access === 'mtts'
          raise APIErrors::UnprocessableEntity.new(error_message: "You can only access MTT game.") if strategy_selection_params[:game_type] != 'mtt'
        end
      end
    end

    def check_existing_channel
      response = HTTParty.get check_existing_channels_url
      data = JSON.parse(response.body, symbolize_names: true)

      if data[:number_of_open_channels] && data[:number_of_open_channels].to_i >= 1
        channel_id = data[:channels]['0'.to_sym]
        free_tree(channel_id) if channel_id
        data[:number_of_open_channels] = 0
      end
    end

    def check_existing_channels_url
      ENV['REQUEST_TREE_URL'] + "?user_id=#{current_user[:api_key]}&action_type=6&env=#{Rails.env}"
    end

    def free_tree channel_id
      free_tree_url = ENV['REQUEST_TREE_URL'] + "?channel_id=#{channel_id}&action_type=2&env=#{Rails.env}"
      response = HTTParty.get free_tree_url
  
      store_into_cache(key: current_user.api_key.to_sym, value: [])
    end

end
