class SubscribersController < ApplicationController
  before_action :check_valid_email, :check_subscriber_existing, only: [:create]

  def create
    @subscriber = Subscriber.create(email: subscribers_params[:email]) unless @subscriber
    render_api(
      data: @subscriber,
      status: :ok,
      serializer: SubscriberSerializer
    )
  end

  private

  def check_valid_email
    render json: {  error_message: 'Invalid email' }, status: :ok if(!is_valid_email?(subscribers_params[:email]))
  end

  def check_subscriber_existing
    @subscriber = Subscriber.find_by(email: subscribers_params[:email])
  end

  def subscribers_params 
    params.require(:subscriber).permit(:email)
  end

  def is_valid_email?(email)
    email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  end
end