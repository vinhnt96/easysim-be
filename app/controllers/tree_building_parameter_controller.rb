class TreeBuildingParameterController < ApplicationController
  def get_tree_info    
    @tree_data = TreeBuildingService.new(params).get_tree_info

    render json: {data: @tree_data, status: :ok}
  end

end