class NodesController < ApplicationController
  include HTTParty

  before_action :get_channel_id, only: [:get_turn_river_data, :get_runouts_explorer_data, :free_tree]
  before_action :ensure_free_channel, only: [:get_turn_river_data, :get_runouts_explorer_data]
  before_action :update_channel_status, only: [:get_turn_river_data, :get_runouts_explorer_data]

  after_action :update_channel_status_to_done, only: [:get_turn_river_data, :get_runouts_explorer_data]
  before_action :clear_channel_info, only: [:free_tree]

  def get_nodes
    nodes = FlopsService.new(params, sim_type, more_detail_columns).get_nodes
    render json: { data: nodes, status: :ok }
  end

  def show_current_node
    result = FlopsService.new(params, sim_type, more_detail_columns).current_node
    render json: { data: result, status: :ok }
  end

  def get_turn_river_data
    response = HTTParty.get request_node_url
    data = JSON.parse response.body

    render json: { data: data, status: :ok }
  end

  def more_details_data
    result = FlopsService.new(params, sim_type, more_detail_columns).more_details_data
    render json: { data: result, status: :ok }
  end

  def next_flops_details_data
    result = FlopsService.new(params, sim_type, more_detail_columns).next_flops_details_data
    render json: { data: result, status: :ok }
  end

  def free_tree
    response = HTTParty.get free_tree_url
    data = JSON.parse(response.body, symbolize_names: true)

    if response.code == 200
      render json: { data: data, status: :ok }
    else
      render json: { data: "{}" }, status: :bad_request
    end
  end

  def get_runouts_explorer_data
    url = ENV['REQUEST_TREE_URL'] + "?channel_id=#{@channel_id}&action_type=3&request_type=#{params[:request_type]}&node=#{params[:nodes]}&env=#{Rails.env}"
    response = HTTParty.get(url)
    if response.code == 200
      render json: { data: response.body, status: :ok }
    else
      render json: { data: "{}" }, status: :bad_request
    end
  end

  private

  def more_detail_columns
    case params[:view]
    when 'strategy-ev', 'ev_ip', 'ev_oop', 'compare-ev'
      ", ev_ip, ev_oop, ev_ip2, ev_oop2"
    when 'equity_ip', 'equity_oop'
      ", equity_ip, equity_oop"
    else
      ",#{params[:view]}" if params[:view].present?
    end
  end

  def request_node_url
    ENV['REQUEST_TREE_URL'] + "?channel_id=#{@channel_id}&action_type=1&node=#{params[:nodes]}&env=#{Rails.env}"
  end

  def free_tree_url
    ENV['REQUEST_TREE_URL'] + "?channel_id=#{@channel_id}&action_type=2&env=#{Rails.env}"
  end

  def sim_type
    SIM_TYPES_MAPPING[params[:sim_type]] || params[:sim_type]
  end

  def sims_loaded
    Rails.cache.fetch(current_user.api_key.to_sym) || []
  end

  def ensure_free_channel
    request_info = sims_loaded.find{|sim| sim[:channel_id] == params[:channel_id]} if sims_loaded.present?

    raise APIErrors::ChannelIsBusy if request_info && request_info[:status] == 'processing'
  end

  def update_channel_status
    sims = sims_loaded.map do |sim|
      sim[:channel_id] != @channel_id ? sim : { channel_id: @channel_id, status: 'processing' }
    end
    store_into_cache(key: current_user.api_key.to_sym, value: sims)
  end

  def update_channel_status_to_done
    sims = sims_loaded.map{|sim| sim[:channel_id] != @channel_id ? sim : {channel_id: @channel_id, status: 'done'}}

    store_into_cache(key: current_user.api_key.to_sym, value: sims)
  end

  def clear_channel_info
    sims = sims_loaded.select{|sim_loaded| sim_loaded[:channel_id] != @channel_id} if sims_loaded.length > 0
    store_into_cache(key: current_user.api_key.to_sym, value: sims)
  end

  def get_channel_id
    @channel_id = params[:channel_id]

    raise APIErrors::ResourceNotFound.new(error_message: "Missing channel id") unless @channel_id
  end

end
