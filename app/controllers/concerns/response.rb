module Response
  extend ActiveSupport::Concern

  def render_api(data:, serializer: nil, scope: {}, params: {}, meta: nil, status: 200, extra_data: {})
    return render(json: data, adapter: :json, root: :data) if serializer.nil?

    if is_relation?(data) || data.is_a?(Array)
      if data.respond_to?(:current_page)
        response.headers.merge!(
          'X-Pagination-Page' => data.current_page.to_s,
          'X-Pagination-Total-Records' => data.total_count.to_s,
          'X-Pagination-Total-Pages' => data.total_pages.to_s,
          'X-Pagination-Per-Page' => predicate.pagination.per_page.to_s,
          'X-Pagination-Previous-Page' => data.prev_page.to_s,
          'X-Pagination-Next-Page' => data.next_page.to_s,
          'X-Sorting' => sorting.encoded
        )
        Easysim::Api::Pagination.patch(response, data)
      end
    end

    render_params = {
      json: is_model_activity?(data) ? data.flatten : data,
      adapter: :json,
      "#{serializer_type(data)}": serializer,
      root: nil,
      scope: scope,
      params: params,
      meta: meta,
      status: status 
    }
    render_params.merge!(extra_data)

    render(render_params)
  end

  def render_error(status, message, data = nil)
    response = {
      success: false,
      message: message
    }
    response = response.merge(data) if data

    render_params = {
      json: response,
      status: status,
      adapter: :json,
      root: false,
      scope: {}
    }
    render(render_params)
  end

  private

  def is_relation?(data)
    data.is_a?(ActiveRecord::Relation)
  end

  def is_model_activity?(data)
    data.present? && data.is_a?(Array) && data[0].is_a?(Array)
  end

  def serializer_type(data)
    (is_relation?(data) || data.is_a?(Array)) ? :each_serializer : :serializer
  end
end
