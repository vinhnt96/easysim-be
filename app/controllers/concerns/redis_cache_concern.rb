module RedisCacheConcern
  extend ActiveSupport::Concern

  def store_into_cache key:, value:, expires_in: 1.hours
    Rails.cache.write(key.to_sym, value, :expires_in => expires_in)
  end

end
  