class PaymentwallsController < ApplicationController

  def widget_call
    paymentwall_checkout_url = PaymentwallService.new(current_user).paymentwall_widget
    render json: { data: paymentwall_checkout_url, status: :ok}
  end

  def pingback_process
    PaymentwallService.new(pingback_params: pingback_params).pingback_process
  end

  private

  def pingback_params
    params.permit(:uid, :goodsid, :slength, :speriod, :type, :ref, :is_test, :sign_version, :sig, :price )
  end

end
