class LoadTreesController < ApplicationController
  include HTTParty

  before_action :ensure_have_fully_params, only: [:create]

  def create
    response = HTTParty.get load_tree_url
    data = JSON.parse(response.body, symbolize_names: true)
    update_channel_status data

    render json: { data: data, status: :ok }
  end

  def existing_channels
    response = HTTParty.get check_existing_channels_url
    data = JSON.parse(response.body, symbolize_names: true)

    if data[:number_of_open_channels] && data[:number_of_open_channels].to_i >= 1 && !current_user.has_role?('admin')
      channel_id = Rails.cache.fetch(current_user.api_key.to_sym) || ''
      free_tree(channel_id) if channel_id
      data[:number_of_open_channels] = 0
    end

    render json: { data: data, status: :ok }
  end

  private

  def sims_loaded
    Rails.cache.fetch(current_user.api_key.to_sym) || []
  end

  def load_tree_url
    sim_name = LoadTreeRequestService.new(params: params).get_sim_name
    ENV['REQUEST_TREE_URL'] + "?user_id=#{current_user[:api_key]}&action_type=0&sim_name=#{sim_name}&env=#{Rails.env}"
  end

  def check_existing_channels_url
    ENV['REQUEST_TREE_URL'] + "?user_id=#{current_user[:api_key]}&action_type=6&env=#{Rails.env}"
  end

  def is_fully_params
    params[:game_type].present? &&
    params[:positions].present? &&
    params[:sim_type].present? &&
    params[:flop_cards].present?
  end

  def ensure_have_fully_params
    raise APIErrors::MissingParameters.new(params_name: '') unless is_fully_params
  end

  def ensure_free_channel
    unless current_user.has_role?('admin') || sims_loaded.size.zero?
      existing_channel = sims_loaded[0]
      free_tree existing_channel[:channel_id] if existing_channel
    end
  end

  def update_channel_status data
    sims = sims_loaded << { channel_id: data[:channel_id], status: 'done' }

    store_into_cache(key: current_user.api_key.to_sym, value: sims)
  end

  def free_tree channel_id
    free_tree_url = ENV['REQUEST_TREE_URL'] + "?channel_id=#{channel_id}&action_type=2&env=#{Rails.env}"
    response = HTTParty.get free_tree_url

    store_into_cache(key: current_user.api_key.to_sym, value: [])
  end

end
