class StacksizesController < ApplicationController
  def get_initial_stacksize
    stacksize = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      stacksize = Stacksize.where("filename ILIKE ?", "#{params[:sim]}#{specs}%").first
      unless stacksize
        stacksize = Stacksize.where("filename ILIKE ?", "#{params[:sim_reverse]}#{specs}%").first
      end
    end

    render_api(
      data: stacksize,
      status: :ok,
      serializer: StacksizeSerializer
    )
  end

  private

  def specs
    return '-25x8mmtts' if params[:stack_size].to_i == 100 && (params[:game_type] == 'mtt' || params[:specs] == '8max')
    return '' if params[:game_type] == 'mtt' && params[:stack_size].to_i != 100
    params[:specs] == 'hu' ? '-25xhus10ks' : '-25x6m500zs'
  end
end
