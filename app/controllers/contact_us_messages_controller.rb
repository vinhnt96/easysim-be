class ContactUsMessagesController < ApplicationController

  def create
    @contact_us_message = ContactUsMessage.new(message_params)

    if @contact_us_message.save
      render_api(
        data: {
          message: 'Your message has been sent successfully',
        },
        status: :created
      )
    else
      render json: @contact_us_message.errors, status: :unprocessable_entity
    end
  end

  private

  def message_params
    params.require(:contact_message).permit(:name, :email, :message)
  end
end
