class PlansController < ApplicationController
  skip_before_action :authenticate_user!
  def index
    @plans = Plan.all

    render_api(
      data: @plans,
      status: :ok,
      serializer: PlanSerializer,
    )
  end

end
