class HandCategoriesController < ApplicationController
  def get_hand_category
    hand_category = nil    
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      hand_category = HandCategory.find_by(board: params[:board])
    end

    render_api(
      data: hand_category,
      status: :ok,
      serializer: HandCategorySerializer
    )
  end

end