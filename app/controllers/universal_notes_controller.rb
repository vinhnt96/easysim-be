class UniversalNotesController < ApplicationController
  def get_data
    universal_note = get_universal_note 

    render_api(
      data: universal_note,
      status: :ok,
      serializer: UniversalNoteSerializer,
    )    
  end

  def update_data
    universal_note = get_universal_note 
    universal_note.update(note_data: params[:note_data].to_json)

    render_api(
      data: universal_note.user,
      status: :ok,
      serializer: UserSerializer,
    )  
  end

  private
  def get_universal_note    
    current_user.universal_note || 
    current_user.create_universal_note(
      note_data: {
        default_note: "",
        notes: []
      }.to_json
    )
  end
end