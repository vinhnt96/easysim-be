class BugReportsController < ApplicationController
  def create
    @bug_report = current_user.bug_reports.create(report_bug_params)

    if @bug_report
      render_api(
        data: {
          message: 'Bug report has been sent successfully',
        },
        status: :created
      )
    else
      render json: @bug_report.errors, status: :unprocessable_entity
    end
  end

  private

  def report_bug_params
    params.require(:report_bug_info).permit(:bug_type, :subject, :description, :page)
  end
end
