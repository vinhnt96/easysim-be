class SubscriptionsController < ApplicationController
  before_action :fastspring_auth, only: [:cancel_current_subscription, :change_subscription]
  before_action :get_fastspring_subscription_id, only: [:cancel_current_subscription, :change_subscription]
  skip_before_action :authenticate_user!, only: [:create]
  
  def create
    result = Subscriptions::Create.new(user_instance).execute(subscription_params)

    render_api(
      data: result,
      status: :ok,
      serializer: UserSerializer
    )
  end

  def index
    my_subscriptions = Admin::Subscriptions::Index.new.call(index_predicate)

    render_api(
      data: my_subscriptions,
      status: :ok,
      serializer: SubscriptionSerializer
    )
  end

  def cancel_current_subscription
    result = Subscriptions::Update.new(current_user, params, @fastspring_auth).cancel_subscription
    render_api(
      data: result,
      status: :ok,
      serializer: UserSerializer
    )    
  end

  def change_subscription
    result = Subscriptions::Update.new(current_user, params, @fastspring_auth).change_subscription_product
    render json: { result: result, status: :ok }
  end

  private

  def user_instance
    should_create_user = params[:not_sign_in].to_s == 'true'

    return User.invite!(email: params[:email], first_name: params[:first_name], last_name: params[:last_name], affiliate_code: params[:affiliate_code]) if should_create_user

    raise APIErrors::Unauthorized.new(error_message: "You are unauthorized to perform this action") unless current_user

    current_user
  end

  def subscription_params
    params.permit(:product_id, :code_discount, :paid, :status, :fastspring_subscription_id, :auto_renew)
  end

  def index_predicate
    sorting.default(start_time: :desc)
    query = {
      user_id: { equal: current_user[:id] },
      email: { like: params[:search] }
    }
    params[:filter] ||= {}
    params[:filter] = params[:filter].merge(query)
    filters.permit(user_id: :equal)
    index_predicate = predicate
    index_predicate.includes_tables = [:user, :plan]
    index_predicate
  end

  def get_fastspring_subscription_id
    params[:fastspring_subscription_id] ||= current_user[:current_subscription_id] ? Subscription.find(current_user[:current_subscription_id])&.fastspring_subscription_id : nil
  end
end
