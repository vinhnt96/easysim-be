class RolesController < ApplicationController

  def index
    role = Role.all
    render_api(
      data: role,
      status: :ok,
      serializer: RoleSerializer,
    )
  end
end
