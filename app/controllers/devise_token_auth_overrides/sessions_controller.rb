module DeviseTokenAuthOverrides
  class SessionsController < DeviseTokenAuth::SessionsController
    def create
      super do |user|
        IpHistory.create(user_id: user.id, ip: user.current_sign_in_ip)
        current_time_utc = DateTime.current
        if !user.current_activity.present?
          user.activities.create({
            logged_time: current_time_utc,
            begin_date: current_time_utc.beginning_of_day
          })
        end
      end
    end
  end
end

