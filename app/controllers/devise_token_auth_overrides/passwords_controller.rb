module DeviseTokenAuthOverrides
  class PasswordsController < DeviseTokenAuth::PasswordsController
    def edit
      @resource = resource_class.with_reset_password_token(resource_params[:reset_password_token])
      if @resource && @resource.reset_password_period_valid?
        super
      else
        redirect_to Rails.application.config.client_url + "/forgot-password?status=expired"
      end
    end

    def update
      if params[:email].present?
        result = DeviseTokenAuthOverrides::Passwords::Update.new(user).excute(params_permit)
        render json: result
      else
        super
      end
    end

    private

    def user
      User.find_by_email(params[:email]) if params[:email].present?
    end

    def params_permit
      params.permit(:current_password, :password, :confirm_password)
    end

  end
end
