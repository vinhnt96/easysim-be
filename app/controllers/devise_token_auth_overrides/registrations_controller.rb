module DeviseTokenAuthOverrides
  class RegistrationsController < DeviseTokenAuth::RegistrationsController
    def create
      if User.where(email: user_params["email"]).exists?
        render json: {
          errors: { email: "This email has already been taken." }
        }, status: :bad_request
      else
        super
        user = User.find(resource_data['id'])
        user.roles = [role]
      end
    end

    private

    def render_create_success
      render json: {
        status: 'success',
        data: UserSerializer.new(User.find(resource_data['id'])).as_json
      }
    end

    def user_params
      params.require(:registration).permit(:email, :password, :first_name, :last_name, :birthday, :country, :affiliate_code)
    end

    def role
      Role.find_by_name('free_user')
    end
  end
end
