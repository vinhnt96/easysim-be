module DeviseTokenAuthOverrides
  class InvitationsController < Devise::InvitationsController
    respond_to :json
    before_action :resource_from_invitation_token, only: :update
    skip_before_action :authenticate_inviter!, :has_invitations_left?

    def create
      user = User.find_by(email: invitation_params[:email])
      if user.blank?
        user = User.invite!(
          {email: invitation_params[:email], affiliate_code: invitation_params[:affiliate_code]},
          nil,
          {not_subscription: true}
        )
        render_api(
          data: user,
          status: :ok,
          serializer: UserSerializer
        )
      else
        render json: { status: 402, message: 'User existing.' }, status: 402, scope: {}
      end
    end

    def edit
      new_user = User.find_by_invitation_token(params[:invitation_token], true)

      if new_user
        redirect_headers = {invitation_token: params[:invitation_token]}
        redirect_to(build_auth_url(params[:redirect_url], redirect_headers))
      else
        render_error(404, "Invalid invitation token")
      end
    end

    def invitation_token
      @user = resource_class.find_by_invitation_token(params[:invitation_token], true)

      if @user.present?
        render_api(
          data: @user,
          status: :ok,
          serializer: UserSerializer
        )
      else
        render_error(404, "Invalid invitation token")
      end
    end

    def update
      user = User.accept_invitation!(accept_invitation_params)
      if user
        render_api(
          data: user,
          status: :ok,
          serializer: UserSerializer
        )
      else
        render_error(401, "Invalid")
      end
    end

    private

    def invitation_params
      params.require(:invitation).permit(:email, :affiliate_code)
    end

    def build_auth_url(base_url, args)
      DeviseTokenAuth::Url.generate(base_url, args)
    end

    def resource_from_invitation_token
      return if params[:invitation_token] &&
                User.find_by_invitation_token(params[:invitation_token], true)
      render_error(401, "Invalid token.")
    end

    def accept_invitation_params
      params.permit(:id, :invitation_token, :password, :password_confirmation, :first_name, :last_name, :birthday, :country)
    end
  end
end
