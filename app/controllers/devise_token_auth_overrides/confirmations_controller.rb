module DeviseTokenAuthOverrides
  class ConfirmationsController < DeviseTokenAuth::ConfirmationsController
    def show
      @resource = resource_class.confirm_by_token(resource_params[:confirmation_token])

      if @resource.errors.empty?
        yield @resource if block_given?

        redirect_header_options = { account_confirmation_status: 'success' }

        if signed_in?(resource_name)
          token = signed_in_resource.create_token
          signed_in_resource.save!

          redirect_headers = build_redirect_headers(token.token,
                                                    token.client,
                                                    redirect_header_options)

          redirect_to_link = signed_in_resource.build_auth_url(redirect_url, redirect_headers)
        else
          redirect_to_link = DeviseTokenAuth::Url.generate(redirect_url, redirect_header_options)
        end

        redirect_to(redirect_to_link)
      else
        if @resource.confirmed?
          status = 'confirmed'
        elsif confirmation_period_expired?
          status = 'expired'
        else
          status = 'failed'
        end

        redirect_to Rails.application.config.client_url + "/signin?account_confirmation_status=#{status}"
      end
    end

    private

    def confirmation_period_expired?
      @resource.class.confirm_within && @resource.confirmation_sent_at && (Time.now.utc > @resource.confirmation_sent_at.utc + @resource.class.confirm_within)
    end
  end
end
