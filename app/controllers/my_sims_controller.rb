class MySimsController < ApplicationController

  def index
    @user_strategies_list = MySims::Index.new().call(index_predicate)
    @user_strategies_list = MySims::FilterAndOrder.new(@user_strategies_list, params).call

    render_api(
      data: @user_strategies_list,
      status: :ok,
      serializer: MySimSerializer,
    )
  end

  def update
    user_strategy_selection = UserStrategySelection.find(params[:id])
    is_noted = MySims::Index.new().is_noted? params[:notes]
    user_strategy_selection.update(notes: params[:notes], is_noted: is_noted)

    render_api(
      data: user_strategy_selection,
      status: :ok,
      serializer: MySimSerializer,
    )
  end

  def delete_sims
    user_strategies_list = params[:delete_type] == "all" ? current_user.user_strategy_selections
                                                        : current_user.user_strategy_selections.where(id: [params[:id_list]])
    user_strategies_list.update_all(active: false)
    
    render_api(
      data: user_strategies_list,
      status: :ok,
      serializer: MySimSerializer,
    )
  end
    
  private
  def index_predicate
    sorting.default(date_last_view: :desc)
    params[:filter] ||= {}
    if params[:user_id].present?
      sim_ids = params[:sim_ids].present? ? params[:sim_ids].join(",") : ""
      query = { user_id: { equal: params[:user_id] }, id: { in: sim_ids }}
      params[:filter] = params[:filter].merge(query)
      filters.permit( user_id: :equal, id: :in )
    else
      query = { user_id: { equal: current_user[:id] }, active: { equal: true }}
      params[:filter] = params[:filter].merge(query)
      filters.permit(user_id: :equal, active: :equal)
    end
    index_predicate = predicate
    index_predicate.includes_tables = [:strategy_selection]
    index_predicate
  end

  def sims_notes_params
    params.require(:my_sims).permit(:notes, :id, :date_last_view)
  end
end
