class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  include Response
  include RedisCacheConcern
  include HTTParty

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, unless: :devise_controller?

  rescue_from APIError, with: :raise_error

  def raise_error(exception)
    render json: exception.to_hash, status: :ok
  end

  def user
    User.find(params.fetch(:id))
  end

  private

  def configure_permitted_parameters
    @permit_params = [:first_name, :last_name, :birthday, :country, :affiliate_code]
    devise_parameter_sanitizer.permit(:sign_up, keys: @permit_params )
    devise_parameter_sanitizer.permit(:account_update, keys: @permit_params )
    devise_parameter_sanitizer.permit(:accept_invitation, keys: @permit_params )
  end

  def init_paypal_api_access_token
    @paypal_api_access_token = Rails.cache.fetch(:paypal_api_access_token)
  end

  def work_with_auto_renew_paypal_api_access_token(&block)
    begin
      yield
    rescue StandardError => e
      @paypal_api_access_token = GetPaypalApiAccessTokenService.call
      Rails.cache.write(:paypal_api_access_token, @paypal_api_access_token)
      yield
    end
  end

  def predicate
    Easysim::Api::Predicate.new(
      sorting: sorting,
      pagination: pagination,
      filters: filters.set(filters_params.permit!.to_h.deep_symbolize_keys)
    )
  end

  def sorting
    @sorting ||= Easysim::Api::Sorting.new(sorting_params)
  end

  def pagination
    @pagination ||= Easysim::Api::Pagination.new(
      pagination_params.to_h.deep_symbolize_keys
    )
  end

  def filters
    @filters ||= Easysim::Api::Filters.new
  end

  def sorting_params
    params.permit(:sort).fetch(:sort, '')
  end

  def pagination_params
    params.permit(:page, :per_page)
  end

  def filters_params
    params.permit(filter: filters.permitted).fetch(:filter, {})
  end

  def fastspring_auth
    @fastspring_auth ||= Fastspring::BaseService.get_credentials
  end
end
