class PreflopsController < ApplicationController
  def get_preflop_data
    preflop = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      preflop = Preflop.find_by(
        game_type: game_type,
        stack_size: params[:stack_size],
        node: params[:node]
      )
    end

    if preflop.present?
      render json: preflop, serializer: PreflopSerializer
    else
      render json: { game_ended: true }
    end
  end

  private

  def game_type
    if params[:game_type] == 'cash' && params[:specs] == '8max'
      'mtt'
    else
      params[:game_type]
    end
  end
end
