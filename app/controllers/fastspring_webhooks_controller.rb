class FastspringWebhooksController < ApplicationController
  skip_before_action :authenticate_user!

  def order_completed
    FastspringWebhooksService.new().order_completed_processor(params)
  
    render json: { status: 'ok' }, status: :ok
  end

  def subscription_canceled
    updated_success = FastspringWebhooksService.new().subscription_canceled_processor(params)
    render json: { status: 'ok' }, status: :ok
  end

  def order_failed
    FastspringWebhooksService.new().order_failed_processor(params)
    render json: { status: 'ok' }, status: :ok
  end

  def subscription_deactivated
    FastspringWebhooksService.new().subscription_deactivated_processor(params)
    render json: { status: 'ok' }, status: :ok
  end

  def subscription_charge_completed
    FastspringWebhooksService.new().subscription_charge_completed_processor(params)
    render json: { status: 'ok' }, status: :ok
  end

  def subscription_charge_failed
    FastspringWebhooksService.new().subscription_charge_failed_processor(params)
    render json: { status: 'ok' }, status: :ok
  end

  def subscription_updated
    FastspringWebhooksService.new().subscription_updated_processor(params)
    render json: { status: 'ok' }, status: :ok
  end
end
