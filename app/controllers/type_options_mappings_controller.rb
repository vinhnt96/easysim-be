class TypeOptionsMappingsController < ApplicationController

  def get_type_options
    positions_params = "{#{params[:positions]&.join(',')}}"
    type_options = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      type_options = TypeOptionsMapping.where(
        string_query,
        game_type: params[:game_type],
        stack: params[:stack],
        specs: params[:specs],
        positions: positions_params
      ).first&.type_options
    end

    render json: { type_options: type_options }
  end

  private 
  
  def string_query
    params[:specs].blank? ? 'game_type = :game_type AND stack = :stack AND positions @> :positions AND :positions @> positions' :
    "game_type = :game_type AND stack = :stack AND specs = :specs AND positions @> :positions AND :positions @> positions"
  end

end
