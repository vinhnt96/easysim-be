class Admin::BaseController < ApplicationController
  before_action :authenticate_admin

  private

  def admin_role
    current_user.has_role? 'admin'
  end

  def authenticate_admin
    raise APIErrors::Unauthorized.new(
      error_message: "You are unauthorized to perform this action",
    ) unless admin_role
  end
end