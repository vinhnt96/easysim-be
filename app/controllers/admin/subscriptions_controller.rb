class Admin::SubscriptionsController < Admin::BaseController

  def index
    @list_admin_id = User.includes(:roles).where(roles: {name: :admin}).pluck(:id)
    subscriptions = Admin::Subscriptions::Index.new.call(index_predicate)
    subscriptions = Admin::Subscriptions::FilterAndOrder.new(subscriptions, params).call

    render_api(
      data: subscriptions,
      status: :ok,
      serializer: SubscriptionSerializer
    )
  end

  private

  def index_predicate
    sorting.default(start_time: :desc)
    params[:filter] ||= {}
    query = { user_id: { not_in: @list_admin_id.join(",") }, email: { like: params[:search] } }
    query = query.merge({ status: { equal: 0 }}) if params[:status].present?    # just get active subs for now.
    params[:filter] = params[:filter].merge(query)
    filters.permit(user_id: :not_in, status: :equal)
    index_predicate = predicate
    index_predicate.includes_tables = [:user, :plan]
    index_predicate
  end


end