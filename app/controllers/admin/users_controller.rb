class Admin::UsersController < Admin::BaseController
  before_action :user_exists?, except: [:index]

  def index
    result = Admin::Users::Index.new(current_user).call(index_predicate, params[:sort])
    render_api(
      data: result,
      status: :ok,
      serializer: UserSerializer,
    )
  end

  def update
    result = Admin::Users::Update.new(user).excute(user_params)
    render_api(
      data: result,
      status: :ok,
      serializer: UserSerializer,
    )
  end

  def destroy
    result = user.destroy!
    render json: { user: result }
  end

  def resent_confirm_email
    Admin::Users::ResentEmail.new(user).excute
    render json: { success: :ok }
  end

  private

  def user_params
    params.permit(:first_name, :last_name, :country, :birthday, :email, :role_id, :current_password, :password, :confirm_password, :user_type, :affiliate_code, :accessibility_game_type)
  end

  def user_exists?
    raise APIErrors::ResourceNotFound.new(error_message: "User not found") unless user
  end

  def user
    @user ||= User.find(params[:id])
  end

  def index_predicate
    sorting.default(id: :asc)
    params[:filter] ||= {}
    query = {}
    query[:email] = { like: params[:search] } if params[:search].present?
    params[:filter] = params[:filter].merge(query)
    filters.permit(id: :not_equal, email: :like)
    predicate
  end

end
