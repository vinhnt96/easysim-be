class Admin::ActivitiesController < Admin::BaseController
  before_action :user_exists?

  def index
    result = Admin::Activities::Index.new(user).call(predicate)
    render_api(
      data: result,
      status: :ok,
      serializer: ActivitySerializer,
    )
  end

  private

  def user
    @user ||= User.find(params[:user_id])
  end

  def user_exists?
    raise APIErrors::ResourceNotFound.new(error_message: "User not found") unless user
  end
end
