class Admin::Paypal::PlansController < Admin::BaseController
  before_action :init_paypal_api_access_token, only: [:index, :create]

  def index
    work_with_auto_renew_paypal_api_access_token do
      raise APIErrors::MissingPaypalAPIAccessToken unless @paypal_api_access_token

      body = Admin::Paypal::PlanService.new(@paypal_api_access_token).get_list
      render json: { data: JSON.parse(body) }
    end
  end

  def create
    work_with_auto_renew_paypal_api_access_token do
      raise APIErrors::MissingPaypalAPIAccessToken unless @paypal_api_access_token

      response = Admin::Paypal::PlanService.new(@paypal_api_access_token, create_plan_params).create

      raise APIErrors::UnprocessableEntity.new(error_message: "Create new plan(s) failed") if response.code != 201

      render json: { data: JSON.parse(response.body) }
    end
  end

  def destroy

  end

  private

  def create_plan_params
    {
      product_id: ENV['PAYPAL_PRODUCT_ID'],
      name: "Basic Subscription Plan",
      description: "Basic subscription plan",
      status: "ACTIVE",
      billing_cycles: [
        {
          frequency: {
            interval_unit: "MONTH",
            interval_count: 1
          },
          tenure_type: "REGULAR",
          sequence: 1,
          total_cycles: 12,
          pricing_scheme: {
            fixed_price: {
              value: "2",
              currency_code: "USD"
            }
          }
        }
      ],
      payment_preferences: {
        auto_bill_outstanding: true,
        setup_fee: {
          value: "2",
          currency_code: "USD"
        },
        setup_fee_failure_action: "CONTINUE",
        payment_failure_threshold: 3
      },
      taxes: {
        percentage: "10",
        inclusive: false
      }
    }
  end
end