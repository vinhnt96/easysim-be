class BaseWorker
  include Sidekiq::Worker

  sidekiq_options :retry => 5
end
