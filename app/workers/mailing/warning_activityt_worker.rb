module Mailing
  class WarningActivitytWorker < Mailing::BaseMailerWorker
    def perform email
      WarningActivityMailer.send_warning_activity_email(email).deliver_now
    end
  end
end
