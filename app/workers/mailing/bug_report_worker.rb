module Mailing
  class BugReportWorker < Mailing::BaseMailerWorker
    def perform report_id
      report = BugReport.find_by(id: report_id)

      BugReportMailer.send_report_bug_email(report).deliver_now
    end
  end
end
