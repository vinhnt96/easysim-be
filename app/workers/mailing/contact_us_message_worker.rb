module Mailing
  class ContactUsMessageWorker < Mailing::BaseMailerWorker
    def perform contact_message_id
      message = ContactUsMessage.find_by(id: contact_message_id)

      ContactUsMailer.send_contact_message(message).deliver_now
    end
  end
end
