module MySims
  class Index
    def call predicate
      predicate.apply_on(UserStrategySelection)
    end
    
    def convert_flops_key flops_params
      flops = flops_params.is_a?(Array) ? flops_params.join('').downcase : flops_params.downcase
      delimiters = ["s","d","c","h"]
      flops_rank_array = flops.split(Regexp.union(delimiters))
      re = Regexp.union(CONVERT_FLOPS_MAP.keys)
      flops_key = flops.gsub(re, CONVERT_FLOPS_MAP)
      flops_key
    end

    def is_noted? notes
      if notes.present?
        notes.each do |view, value|
          notes[view].each do |first_key, value|
            if notes[view][first_key].is_a?(String)
              return true if notes[view][first_key].length > 0
            else
              notes[view][first_key].each do |second_key, value|
                return true if notes[view][first_key][second_key].length > 0
              end
            end
          end
        end
      end
      
      false
    end
    
  end
end