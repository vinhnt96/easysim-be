module MySims
  class FilterAndOrder
    attr_reader :search_params
    attr_accessor :user_strategies_list
    attr_accessor :sort_column

    def initialize user_strategies_list, params
      @user_strategies_list = user_strategies_list
      @search_params = params[:search]
      @sort_column = params[:sort_column]
    end

    def call
      if @search_params.present? || @sort_column.present?
        flops_key = MySims::Index.new().convert_flops_key @search_params.downcase
        @user_strategies_list = @user_strategies_list.joins(:strategy_selection)
                                .where("LOWER(strategy_selections.flops_key) LIKE ? ", "%#{flops_key}%").reorder(order_params)
      end
      @user_strategies_list
    end

    private
    def order_params
      delimiters = ["_asc", "_desc"]
      column = @sort_column.gsub(Regexp.union(delimiters),'')
      direction = @sort_column.split('_').pop

      sort_params = "#{convert_column(column)} #{direction}"
    end

    def convert_column column
      real_column = case column
      when "board"
        "strategy_selections.flops_key"
      when "positions"
        "strategy_selections.positions_key"
      when "stack_size"
        "strategy_selections.stack"
      when "pot_type"
        "strategy_selections.sim_type"
      when "game_type"
        "strategy_selections.game_type"
      when "notes"
        "is_noted"
      else
        "date_last_view"
      end
    end

  end
end
