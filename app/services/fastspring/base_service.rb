module Fastspring
  class BaseService
    def self.get_credentials
      { username: ENV['FASTSPRING_CLIENT_ID'], password: ENV['FASTSPRING_CLIENT_SECRET'] }
    end
  end
end