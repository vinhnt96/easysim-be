module Fastspring
  class SubscriptionService
    include HTTParty
    base_uri ENV['FASTSPRING_BASE_URI']

    attr_reader :fastspring_auth

    def initialize params = {}, fastspring_auth = {}
      @params = params
      @payload = {basic_auth: fastspring_auth}
    end

    def get_all
      url_with_params = "/subscriptions?"
      url_with_params += "&limit=#{@params[:limit]}" if @params[:limit]
      url_with_params += "&begin=#{@params[:begin]}" if @params[:begin]
      url_with_params += "&end=#{@params[:end]}" if @params[:end]
      url_with_params += "&scope=#{@params[:scope]}" if @params[:scope]

      response = fastspring_call('get', url_with_params)
    end

    def get
      fastspring_call('get', "/subscriptions/#{@params[:fastspring_subscription_id]}")
    end

    def cancel
      fastspring_call('delete', "/subscriptions/#{@params[:fastspring_subscription_id]}")
    end

    def reverse_cancel
      body = { 
        subscriptions:[  
          { subscription: @params[:fastspring_subscription_id], deactivation: nil }
        ]
      }
      @payload[:body] = JSON.dump(body)
      fastspring_call('post', '/subscriptions')
    end

    def deactivate
      fastspring_call('delete', "/subscriptions/#{@params[:fastspring_subscription_id]}?billingPeriod=0")
    end

    def change_product
      body = {
        subscriptions: [
          { subscription: @params[:fastspring_subscription_id], product: @params[:product_id], prorate: true }
        ]
      }
      @payload[:body] = JSON.dump(body)
      fastspring_call('post', '/subscriptions')
    end

    private
    def fastspring_call http_type, url
      case http_type
        when 'get'
          return self.class.get(url, @payload)
        when 'post'
          return self.class.post(url, @payload)
        when 'put'
          return self.class.put(url, @payload)
        when 'delete'
          return self.class.delete(url, @payload)
        else
          raise ArgumentError.new("HTTP type params have invalid value")
        end
    end

  end
end