class FlopsService

  def initialize params, sim_type, more_detail_columns
    @params = params
    @sim_type = sim_type
    @more_detail_columns = more_detail_columns
  end

  def get_nodes
    nodes = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      sql = " SELECT id,
                nodes,
                node_count,
                pot_ip,
                pot_oop,
                pot_main,
                strategy,
                num_combos_oop,
                num_combos_ip,
                ranges_ip,
                ranges_oop,
                num_combos_ip,
                num_combos_oop
                #{@more_detail_columns}
              FROM flops
              WHERE game_type='#{game_type}'
              AND stack_size='#{@params[:stack_size]}'
              AND (
                positions='#{@params[:positions]}'
                OR positions='#{@params[:positions].split('v').reverse().join('v')}'
              )
              AND sim_type='#{@sim_type}'
              AND flop_cards='#{@params[:flops]}'
              AND node_count='#{@params[:node_count]}'
              AND nodes ILIKE '#{@params[:nodes]}:%'
              #{specs}
            "

      nodes = ApplicationRecord.connection.execute(sql)
    end
    nodes
  end

  def current_node
    result = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      sql = "SELECT id,
              nodes,
              node_count,
              pot_ip,
              pot_oop,
              pot_main,
              strategy,
              num_combos_oop,
              num_combos_ip,
              ranges_ip,
              ranges_oop,
              num_combos_ip,
              num_combos_oop,
              CASE
                WHEN nodes = '#{@params[:nodes]}' THEN 1
                ELSE 0
              END as sort_order
              #{@more_detail_columns}
            FROM flops
            WHERE game_type='#{game_type}'
            AND stack_size='#{@params[:stack_size]}'
            AND (
              positions='#{@params[:positions]}'
              OR positions='#{@params[:positions].split('v').reverse().join('v')}'
            )
            AND sim_type='#{@sim_type}'
            AND flop_cards='#{@params[:flops]}'
            AND (
              (
                node_count='#{@params[:node_count]}'
                AND nodes = '#{@params[:nodes]}'
              )
              OR (
                node_count='#{@params[:node_count].to_i+1}'
                AND nodes ILIKE '#{@params[:nodes]}:%'
              )
            )
            #{specs}
            ORDER BY sort_order DESC"
      current_flop = ApplicationRecord.connection.execute(sql)
      result = current_flop.first.merge({next_flops: current_flop.drop(1)})
    end
    result
  end

  def next_flops_details_data
    result = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      sql = "SELECT
              nodes,
              CASE
                WHEN nodes = '#{@params[:nodes]}' THEN 1
                ELSE 0
              END as sort_order
              #{@more_detail_columns}
            FROM flops
            WHERE game_type='#{game_type}'
            AND stack_size='#{@params[:stack_size]}'
            AND (
              positions='#{@params[:positions]}'
              OR positions='#{@params[:positions].split('v').reverse().join('v')}'
            )
            AND sim_type='#{@sim_type}'
            AND flop_cards='#{@params[:flops]}'
            AND (
              (
                node_count='#{@params[:node_count]}'
                AND nodes = '#{@params[:nodes]}'
              )
              OR (
                node_count='#{@params[:node_count].to_i+1}'
                AND nodes ILIKE '#{@params[:nodes]}:%'
              )
            )
            #{specs}
            ORDER BY sort_order DESC"
      current_flop = ApplicationRecord.connection.execute(sql)
      result = current_flop.first&.merge({next_flops: current_flop.drop(1)})
    end
    result
  end

  def more_details_data
    result = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      sql = "SELECT #{@more_detail_columns[1..-1]}
            FROM flops
            WHERE game_type='#{game_type}'
            AND stack_size='#{@params[:stack_size]}'
            AND (
              positions='#{@params[:positions]}'
              OR positions='#{@params[:positions].split('v').reverse().join('v')}'
            )
            AND sim_type='#{@sim_type}'
            AND flop_cards='#{@params[:flops]}'
            AND node_count='#{@params[:node_count]}'
            AND nodes='#{@params[:nodes]}'
            #{specs}
            LIMIT 1"
      data = ApplicationRecord.connection.execute(sql)
      result = data.first
    end
    result
  end

  def specs
    @params[:game_type] == 'cash' || (@params[:game_type] == 'mtt' && @params[:stack_size].to_i == 100) ? "AND specs='#{specs_value}'" : ""
  end

  def specs_value
    return '25x8mmtts' if @params[:stack_size].to_i == 100 && (@params[:game_type] == 'mtt' || @params[:specs] == '8max')
    return '' if @params[:game_type] == 'mtt' && @params[:stack_size].to_i != 100
    @params[:specs] == 'hu' ? '25xhus10ks' : '25x6m500zs'
  end

  def game_type
    @params[:game_type] == 'cash' && @params[:specs] == '8max' ? 'mtt' : @params[:game_type]
  end

end
