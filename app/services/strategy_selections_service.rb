class StrategySelectionsService
  attr_reader :params, :user_strategy_selection_item
  attr_accessor :strategy_selection_params
  attr_accessor :user

  def initialize(params={}, user=nil, strategy_selection_params={}, user_strategy_selection_item=nil)
    @params = params
    @user = user
    @strategy_selection_params = strategy_selection_params
    @user_strategy_selection_item = user_strategy_selection_item
  end

  def create_strategy_selection
    @strategy_selection = StrategySelection.where("
      ? = ANY(flops) AND
      ? = ANY(flops) AND
      ? = ANY(flops)",
      @strategy_selection_params[:flops][0],
      @strategy_selection_params[:flops][1],
      @strategy_selection_params[:flops][2],
    ).where("
      ? = ANY(positions) AND
      ? = ANY(positions)",
      @strategy_selection_params[:positions][0],
      @strategy_selection_params[:positions][1],
    ).where(
      street: @strategy_selection_params[:street],
      game_type: @strategy_selection_params[:game_type],
      specs: @strategy_selection_params[:specs],
      stack: @strategy_selection_params[:stack].to_s,
      sim_type: @strategy_selection_params[:sim_type],
      site: @strategy_selection_params[:site],
      stake: @strategy_selection_params[:stake],
      open_size: @strategy_selection_params[:open_size],
    ).first

    unless @strategy_selection
      raise APIErrors::UnprocessableEntity.new(error_message: "Incorrectly params passed") unless is_valid_params

      @strategy_selection = StrategySelection.new(@strategy_selection_params)
      @strategy_selection.flops_key = MySims::Index.new().convert_flops_key @strategy_selection_params[:flops]
      @strategy_selection.positions_key = @strategy_selection_params[:positions].join('')
      @strategy_selection.save
    end

    if @strategy_selection
      @user_strategy_selection = UserStrategySelection.find_or_create_by( 
        strategy_selection_id: @strategy_selection.id, 
        user_id: @user[:id]
      )
      active = @strategy_selection.street == "postflop"
      @user_strategy_selection.update(date_last_view: DateTime.now, active: active)
      @user.update(strategy_selection_id: @strategy_selection.id)
    end

    @strategy_selection
  end

  def get_strategy_selection
    user_strategy_selection_item.update(date_last_view: DateTime.now)
    user.update(strategy_selection_id: user_strategy_selection_item.strategy_selection.id)

    user_strategy_selection_item.strategy_selection
  end

  def is_valid_params
    # check positions are valid for Postflop - CASH - HU game
    if(@strategy_selection_params[:street] == 'postflop' && @strategy_selection_params[:game_type] == 'cash' && @strategy_selection_params[:specs] == 'hu')
      return false unless @strategy_selection_params[:positions].sort == ['b', 'bb']
    end

    # check sim_type is valid
    return false if @strategy_selection_params[:street] == 'postflop' && !get_sim_types.include?(@strategy_selection_params[:sim_type])

    # check number of flop cards
    return false if @strategy_selection_params[:flops].size < 3

    true
  end

  def get_sim_types
    positions_params = "{#{@strategy_selection_params[:positions]&.join(',')}}"
    sim_types = nil
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      sim_types = TypeOptionsMapping.where(
        "game_type = :game_type AND stack = :stack",
        game_type: @strategy_selection_params[:game_type].to_s,
        stack: @strategy_selection_params[:stack].to_s
      ).where("
        ? = ANY(positions) AND
        ? = ANY(positions)",
        @strategy_selection_params[:positions][0],
        @strategy_selection_params[:positions][1],
      )

      if @strategy_selection_params[:game_type] == 'cash' && @strategy_selection_params[:specs] == '8max'
        sim_types = sim_types.where(specs: @strategy_selection_params[:specs])
      end
      sim_types = sim_types.first&.type_options
    end
    sim_types
  end

end
