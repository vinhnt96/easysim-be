class PaypalService
  def initialize *params
    @gateway = PAYPAL_EXPRESS_GATEWAY
    @params = params
  end

  def redirect_url
    setup_response = @gateway.setup_purchase *@params
    @gateway.redirect_url_for(setup_response.token)
  end

  def details
    @gateway.details_for(*@params)
  end

  def purchase
    @gateway.purchase *@params
  end

end
