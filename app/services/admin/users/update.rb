module Admin
  module Users
    class Update

      attr_reader :user

      def initialize user
        @user = user
      end

      def excute(params)
        raise APIErrors::ResourceNotFound.new(error_message: "Could not find user") unless user
        role_id = params.delete(:role_id)
        user.roles = [role(role_id)] if role_id.present?
        password = params.delete(:password)
        confirm_password = params.delete(:confirm_password)
        user.attributes = params if params.present?
        if password.present?
          raise APIErrors::ResourceNotFound.new(error_message: "miss field confirm password") unless confirm_password.present?
          raise APIErrors::ResourceNotFound.new(error_message: "confirm password not match with new password") unless confirm_password === password
          user.reset_password(password, confirm_password)
        end
        user.save
        user
      end

      private

      def role id
        Role.find(id)
      end
    end
  end
end
