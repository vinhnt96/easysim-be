module Admin
  module Users
    class ResentEmail

      attr_reader :user

      def initialize user
        @user = user
      end

      def excute
        token = Devise.friendly_token
        user.confirmation_token = token
        result = user.save
        user.resend_confirmation_instructions if result
      end
    end
  end
end
