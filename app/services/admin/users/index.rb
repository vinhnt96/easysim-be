module Admin
  module Users
    class Index

      attr_reader :user

      def initialize user
        @user = user
      end

      def call(predicate, sort)
        data = User.joins(:roles)
          .select("users.*, roles.id as role_id")
          .where.not(id: user.id)
          .where(predicate.filters.where_clause)
        if sort.include? 'role'
          text_sort = sort.casecmp?('role') ? 'roles.id ASC' : 'roles.id DESC'
          data = data.order(text_sort)
        else
          data = data.order(predicate.sorting.attributes)
        end
        data
          .page(predicate.pagination.page)
          .per(predicate.pagination.per_page)
      end
    end
  end
end
