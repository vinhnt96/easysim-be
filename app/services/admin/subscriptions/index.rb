module Admin
  module Subscriptions
    class Index
      def call predicate
        predicate.apply_on(Subscription)
      end
      
    end
  end
end