module Admin
  module Subscriptions
    class FilterAndOrder
      attr_accessor :subscription_list
  
      def initialize subscription_list, params
        @subscription_list = subscription_list
        @search_params = params[:search]
      end
  
      def call
        result = @search_params.present? ? @subscription_list.joins(:user).where("LOWER(email) LIKE ? ", "%#{@search_params.downcase}%") : @subscription_list
      end
  
    end
  end
end