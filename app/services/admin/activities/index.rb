 module Admin
  module Activities
    class Index

      attr_reader :user

      def initialize user
        @user = user
      end

      def call(predicate)
        return activities unless activities.present?
        dates = activities
          .group_by{ |a| a.logged_time.to_date }
          .values
        Kaminari.paginate_array(dates)
          .page(predicate.pagination.page)
          .per(predicate.pagination.per_page)
      end

      private

      def activities
        @activities ||= user.activities
      end
    end
  end
end
