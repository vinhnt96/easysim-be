module Admin
  module Paypal
    class PlanService
      include HTTParty
      base_uri ENV['PAYPAL_BASE_URI']

      def initialize access_token, params={}
        @headers = { "Authorization" => "Bearer #{access_token}", "Content-Type" => "application/json" }
        @params = params
      end

      def create_product
        params = {
          "name" => "OdinPoker Subscription Service",
          "description" => "OdinPoker subscription service",
          "type" => "SERVICE",
          "category" => "GAME_SOFTWARE",
          "home_url" => "http://www.odinpoker.io"
        }
        # TODO: we should ensure new product is not existing before created
        response = self.class.post( '/catalogs/products', headers: @headers, body: JSON.dump(params) )
        JSON.parse response.body
      end

      def get_list
        response = self.class.get('/billing/plans', headers: @headers)
        if response.code == 200
          response.body
        else
          raise APIErrors::UnprocessableEntity.new(error_message: "Unprocessable Entity")
        end
      end

      def get_details
        response = self.class.get("/billing/plans/#{@params[:plan_id]}", headers: @headers)
        response.body
      end

      def create
        # TODO: we should ensure new plan is not existing before created
        response = self.class.post('/billing/plans', headers: @headers, body: JSON.dump(@params))
        body = JSON.parse response.body
        if response.code == 201
          Plan.find_or_create_by(name: body['name'], paypal_plan_id: body['id']) do |new_plan|
            new_plan.description = body['description']
            new_plan.paypal_product_id = body['product_id']
            new_plan.paypal_status = body['status']
            new_plan.price = @params[:billing_cycles][0][:pricing_scheme][:fixed_price][:value].to_f
            new_plan.currency = @params[:billing_cycles][0][:pricing_scheme][:fixed_price][:currency_code]
            new_plan.billing_period = body['billing_cycles'][0]['frequency']['interval_unit']
            new_plan.billing_frequency = body['billing_cycles'][0]['frequency']['interval_count']
          end
        end
        response
      end

      def update
        # TODO: we should ensure plan is existing before created
        response = self.class.put('/billing/plans', headers: @headers, body: JSON.dump(@params))
        response.body
      end
    end
  end
end

