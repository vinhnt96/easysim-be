class PaymentwallService
  def initialize user=nil, pingback_params={}
    @user = user
    @pingback_params = pingback_params
  end

  def paymentwall_widget
    widget = Paymentwall::Widget.new(
      @user[:id],           # id of the end-user who's making the payment
      'p1_2',        # widget code, e.g. p1; can be picked inside of your merchant account
      [              # product details for Flexible Widget Call. To let users select the product on Paymentwall's end, leave this array empty
        Paymentwall::Product.new(
          'basic_subscription',                            # id of the product in your system
          1,                                               # price
          'USD',                                           # currency code
          'Basic subscription',                            # product name
          Paymentwall::Product::TYPE_SUBSCRIPTION,         # this is a time-based product
          1,                                               # duration is 1
          Paymentwall::Product::PERIOD_TYPE_MONTH,         # month
          true                                             # recurring
        )
      ],
      {
        'email' => 'tom@nustechnology.com',
        'ps'=> 'all',
        'history[registration_date]'=> '2021-04-02',
        'pingback_url'=> 'https://api-dot-bigdongryan-217503.uc.r.appspot.com/paymentwalls/pingback_process',
        'success_url'=> 'http://staging.odinpoker.io',
        'failure_url'=> 'http://staging.odinpoker.io'
      }
    )
    widget.getUrl()
  end

  def pingback_process
    pingback = Paymentwall::Pingback.new(@pingback_params.as_json, request.remote_ip)
    if pingback.validate(true)
      productId = pingback.getProduct.getId
      if pingback.isDeliverable
        # deliver the product
      elsif pingback.isCancelable
        # withdraw the product
      end
      puts 'OK'    # Paymentwall expects response to be OK, otherwise the pingback will be resent
      render json: 'OK', status: 200
    else
      puts pingback.getErrorSummary
    end
  end

end
