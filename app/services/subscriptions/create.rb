module Subscriptions
  class Create
    attr_reader :user

    def initialize user
      @user = user
    end

    def execute(params)
      paid = params[:paid]
      is_success_sub = paid && params[:fastspring_subscription_id].present?
      subscription = Subscription.find_by(fastspring_subscription_id: params[:fastspring_subscription_id])
      return user.reload if is_success_sub && subscription.present?

      product_id = params.delete(:product_id)
      params[:start_time] = Time.current
      params[:plan_id] = get_plan_id(product_id)
      params[:renew_time] = renew_time(params[:plan_id], params[:start_time])
      params[:cycle_time] = cycle_time(params[:plan_id])

      if is_success_sub
        deactivate_current_subscription
        @current_subscription = user.subscriptions.create(params)
        user_type = user.user_type == "promotional" ? "promotional" : "paid"
        user.update(user_type: user_type, current_subscription_id: @current_subscription.id, roles: [role])
      else
        user.subscriptions.create(params) #Create sub failed and don't update current subscription for usser
      end

      user.reload
    end

    private

    def get_plan_id product_id
      Plan.find_by(product_id: product_id)&.id
    end

    def renew_time plan_id, start_time
      plan = Plan.find(plan_id)

      case plan.billing_period
      when 'MONTH'
        return start_time + plan.billing_frequency.months
      else
        return start_time + plan.billing_frequency.years
      end
    end

    def role
      if user.affiliate_code === 'pokercode'
        @role ||= Role.find_by_name('pokercode_user')
      else
        @role ||= Role.find_by_name('odin_user')
      end
    end
    
    def cycle_time plan_id
      plan = Plan.find(plan_id)
      num_of_month = plan.billing_period == 'MONTH' ? 1 : 12
      cycle_time = plan.billing_frequency * num_of_month 
    end

    def deactivate_current_subscription
      subscription = Subscription.find_by(id: user.current_subscription_id)

      if subscription
        if subscription.fastspring_subscription_id
          fastspring_auth ||= Fastspring::BaseService.get_credentials
          Subscriptions::Update.new(
            user,
            {fastspring_subscription_id: subscription.fastspring_subscription_id},
            fastspring_auth
          ).deactivate_subscription
        else
          subscription.update(status: :deactivated, auto_renew: false, paid: false)
        end
      end
    end
  end
end
