module Subscriptions
  class Update
    attr_reader :user
    attr_reader :fastspring_auth

    def initialize user={}, params={}, fastspring_auth={}
      @user = user
      @params = params
      @fastspring_auth = fastspring_auth
    end

    def cancel_subscription
      response = Fastspring::SubscriptionService.new(@params, @fastspring_auth).cancel if @params[:event] == 'canceled'
      updated_subscription = response.present? ? response['subscriptions'][0] : {}
      Subscription.find_by(fastspring_subscription_id:  fastspring_subscription_id)
                  .update(status: :canceled, auto_renew: false) if updated_subscription['result'] == 'success'
      user.reload
    end

    def deactivate_subscription
      response = Fastspring::SubscriptionService.new(@params, @fastspring_auth).deactivate
      updated_subscription = response.present? ? response['subscriptions'][0] : {}
      if updated_subscription['result'] == 'success'
        subscription = Subscription.find_by(fastspring_subscription_id: fastspring_subscription_id)
        subscription.update(status: :deactivated, auto_renew: false, paid: false) if subscription
      end
      user.reload
    end

    def change_subscription_product
      response = Fastspring::SubscriptionService.new(@params, @fastspring_auth).change_product
      updated_subscription = response.present? ? response['subscriptions'][0] : {}
      updated_subscription['result']
    end

    private

    def fastspring_subscription_id
      raise APIErrors::MissingParameters.new(params_name: 'fastspring_subscription_id') unless @params[:fastspring_subscription_id]

      @params[:fastspring_subscription_id]
    end
  end
end
