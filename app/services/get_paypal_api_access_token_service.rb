class GetPaypalApiAccessTokenService
  include HTTParty
  base_uri ENV['PAYPAL_BASE_URI']

  def self.call
    auth = { username: ENV['PAYPAL_CLIENT_ID'], password: ENV['PAYPAL_CLIENT_SECRET'] }
    response = self.post('/oauth2/token', basic_auth: auth, body: {:grant_type => 'client_credentials'})
    if response.code == 200
      body = JSON.parse response.body
      body['access_token']
    else
      raise APIErrors::Unauthorized
    end
  end
end
