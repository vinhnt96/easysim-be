class FastspringWebhooksService

  def initialize
  end

  def order_completed_processor(params)
    event = params[:events][0]
    item = event[:data][:items][0]
    subscription_data = item[:subscription]
    subscription_params = {
      code_discount: event[:data][:coupons][0] || '',
      paid: true,
      status: subscription_data[:state],
      fastspring_subscription_id: subscription_data[:id],
      auto_renew: subscription_data[:autoRenew],
      product_id: item[:product]
    }
    data_tags = event[:data][:tags]
    not_sign_in = data_tags[:not_sign_in]
    user_param = {
      email: data_tags[:user_email],
      first_name: data_tags[:first_name],
      last_name: data_tags[:last_name],
      affiliate_code: data_tags[:affiliate_code]
    }
    user = not_sign_in.to_s == 'true' ? User.invite!(user_param) : User.find_by(email: user_param[:email])
    return true if user.blank?
    
    subscription = Subscription.find_by(fastspring_subscription_id: subscription_params[:fastspring_subscription_id])
    if subscription.present?
      return true
    else
      Subscriptions::Create.new(user).execute(subscription_params)
    end
  end

  def order_failed_processor(params)
    subscription_params = {}
    event = params[:events][0]
    item = event[:data][:items][0]
    subscription_params[:product_id] = item[:product]
    order_starttime = Time.at(event[:data][:changed].to_i/1000).utc
    user = User.find_by(email: event[:data][:tags][:user_email])
    return true if user.blank?

    plan_id = Plan.find_by(product_id: subscription_params[:product_id])&.id
    failed_sub = Subscription.select do |sub|
      start_time = sub.start_time.utc
      same_timespan = DateTime.new(start_time.year, start_time.month, start_time.day, start_time.hour, start_time.min, 0, 'utc') == DateTime.new(order_starttime.year, order_starttime.month, order_starttime.day, order_starttime.hour, order_starttime.min, 0, 'utc')
      same_timespan && sub.user_id == user&.id && sub.plan_id == plan_id && sub.paid == false && sub.status == "failed"
    end
    if failed_sub.present?
      return true
    else
      subscription_params[:code_discount] = event[:data][:coupons][0] || ''
      subscription_params[:paid] = false
      subscription_params[:status] = :failed
      subscription_params[:auto_renew] = false

      Subscriptions::Create.new(user).execute(subscription_params)
    end
  end

  def subscription_canceled_processor(params)
    subscription = get_subscription(params)
    subscription.update(status: :canceled, auto_renew: false, paid: false) if subscription.present? && subscription[:status] != :canceled
  end

  def subscription_deactivated_processor(params)
    subscription = get_subscription(params)
    if subscription.present?
      subscription.update(status: :deactivated, auto_renew: false, paid: false) if subscription[:status] != :deactivated
      user = User.find(subscription[:user_id])
      if user[:current_subscription_id] == subscription[:id]
        user_type = user.user_type == "promotional" ? "promotional" : "free"
        user.update(user_type: user_type, roles: [free_role])
      end
    end
  end
  
  def subscription_charge_completed_processor(params)
    fastspring_sub = params[:events][0][:data][:subscription]
    subscription = charged_subscription(fastspring_sub)
    if subscription
      plan_id = Plan.find_by(product_id: fastspring_sub[:product])&.id
      order_starttime = Time.at(fastspring_sub[:begin].to_i/1000).utc
      start_time = subscription[:start_time].utc
      same_date = DateTime.new(start_time.year, start_time.month, start_time.day, 0, 0, 0, 'utc') == DateTime.new(order_starttime.year, order_starttime.month, order_starttime.day, 0, 0, 0, 'utc')
      start_time_param = same_date ? subscription[:start_time] : Time.current
      renew_time_param = same_date ? subscription[:renew_time] : start_time_param + subscription[:cycle_time].to_i.month
      subscription.update(
        status: fastspring_sub[:state],
        auto_renew: true, paid: true,
        start_time: start_time_param,
        renew_time: renew_time_param,
        plan_id: plan_id
      )
      user = User.find(subscription[:user_id])
      user_type = user.user_type == "promotional" ? "promotional" : "paid"
      user.update(user_type: user_type, roles: [role(user)])
    else
      raise APIErrors::ResourceNotFound.new(error_message: "Subscription not found")
    end
  end

  def subscription_charge_failed_processor(params)
    fastspring_sub = params[:events][0][:data][:subscription]
    subscription = charged_subscription(fastspring_sub)
    if subscription
      charge_for_renew = subscription.plan.product_id == fastspring_sub[:product] #check charge event for renew or upgrade function
      update_params = charge_for_renew ? {status: :expired} : {auto_renew: false}
      subscription.update(update_params)
      if charge_for_renew
        user = User.find(subscription[:user_id])
        user_type = user.user_type == "promotional" ? "promotional" : "free"
        user.update(user_type: user_type, roles: [free_role])
      end
    else
      raise APIErrors::ResourceNotFound.new(error_message: "Subscription not found")
    end
  end

  def subscription_updated_processor(params)
    # comment it for now
    # data = params[:events][0][:data]
    # fastspring_subscription_id = data[:id]
    # subscription = Subscription.find_by(fastspring_subscription_id: fastspring_subscription_id)
    # product_id = data[:product][:product]
    # if subscription.present? && subscription&.plan&.product_id != product_id
    #   plan_id = Plan.find_by(product_id: product_id)&.id
    #   subscription.update(plan_id: plan_id)
    # end
  end

  private

  def get_subscription(params)
    event = params[:events][0]
    fastspring_subscription_id = event[:data][:subscription]
    Subscription.find_by(fastspring_subscription_id: fastspring_subscription_id)
  end

  def charged_subscription(fastspring_sub)
    charged_subscription ||= Subscription.find_by(fastspring_subscription_id: fastspring_sub[:id])
  end

  def role(user)
    if user.affiliate_code === 'pokercode'
      @role ||= Role.find_by_name('pokercode_user')
    else
      @role ||= Role.find_by_name('odin_user')
    end
  end

  def free_role
    @free_role ||= Role.find_by_name('free_user')
  end
end
