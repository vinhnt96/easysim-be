module Easysim
  module Api
    class Sorting

      private

        attr_reader :string

      public

      def initialize(string)
        unless string.is_a?(String)
          raise "paramater must be a string"
        end

        @string = string
        @default = nil
      end

      def default(hash_or_array)
        array = if hash_or_array.is_a?(Hash)
          [hash_or_array]
        else
          hash_or_array
        end

        @default = array
      end

      def attributes
        parts = string.split(",")
        definitions = parts.map do |part|
          direction = part.include?("-") ? :desc : :asc
          attribute = part.gsub("-", "").strip.to_sym
          [attribute, direction]
        end

        definitions = definitions
          .uniq { |predicate| predicate.first }
          .map { |predicate| Hash[predicate.first, predicate.last]  }


        if definitions.none? && default?
          definitions += @default
        end

        if definitions.any?
          definitions
        else
          nil
        end
      end

      def encoded
        Sorting.encode(attributes)
      end

      class << self
        def encode(array)
          if array.is_a?(Hash)
            array = [array]
          end
          (array || [])
            .map { |s| (s.first[1] == :desc ? "-" : "") + s.first[0].to_s }
            .join(",")
        end
      end


      private

        def default?
          @default.present?
        end

    end
  end
end
