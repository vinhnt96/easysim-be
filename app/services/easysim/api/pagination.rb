module Easysim
  module Api
    class Pagination

      class << self
        def patch(response, collection)
          response.headers["X-Pagination"] = header(collection).to_json
        end

        def header(collection)
          {
            current_page: collection.current_page,
            total_count: collection.total_count,
            next_page: collection.next_page,
            previous_page: collection.prev_page,
            pages: collection.total_pages,
            per_page: PER_PAGE
          }
        end
      end

      private

        attr_reader :params

      public

      def initialize(params)
        if params.is_a?(Hash)
          @params = params
        else
          raise "params must be a hash"
        end
      end

      def page
        [params.fetch(:page, 1).to_i, 1].max
      end

      def per_page
        requested_per = params.fetch(:per_page, nil).to_i
        if requested_per <= 0
          requested_per = default_per
        end

        [requested_per, MAX_PER_PAGE].min
      end

      private

        def default_per
          PER_PAGE
        end
    end
  end
end
