module Easysim
  module Api
    class Filters

      attr_reader :permitted
      attr_reader :params

      public

      def initialize
        @params = {}
        @permitted = {}
      end

      def set(_params)
        if _params.is_a?(Hash)
          params.merge!(_params)
          self
        else
          raise "params must be a hash"
        end
      end

      def where_clause
        clauses = params.map do |attribute, condition|
          if permitted?(attribute, condition.keys.first)
            process_condition(attribute, condition)
          end
        end
        build_where_clause(clauses)
      end

      def permit(conditions_hash)
        @permitted.merge!(conditions_hash.symbolize_keys)
        self
      end

      private

        def permitted?(attribute, operator)
          permitted.key?(attribute) && (permitted.fetch(attribute).is_a?(Array) ? permitted.fetch(attribute).include?(operator) : permitted.fetch(attribute) == operator)
        end

        def process_condition(attribute, condition)
          if condition.key?(:like)
            value = condition[:like].to_s.downcase
            unless attribute.match(/\./)
              ["LOWER(#{attribute}) LIKE ?", "%#{value}%"]
            end
          elsif condition.key?(:in)
            value = condition[:in].to_s.split(',')
            ["#{attribute} IN (?)", value]
          elsif condition.key?(:not_in)
            value = condition[:not_in].to_s.split(',')
            ["#{attribute} NOT IN (?)", value]
          elsif condition.key?(:is)
            value = condition[:is] == "1" ? true : false
            ["#{attribute} = ?", value]
          elsif condition.key?(:equal)
            value = condition[:equal]
            ["#{attribute} = ?", value]
          elsif condition.key?(:not_equal)
            value = condition[:not_equal]
            ["#{attribute} != ?", value]
          elsif condition.key?(:not)
            ["#{attribute} IS NOT NULL"]
          elsif condition.key?(:is_null)
            ["#{attribute} IS NULL"]
          else
            nil
          end
        end

        def build_where_clause(clauses)
          clauses = clauses.reject(&:nil?)

          if clauses.any?
            clause = []

            clause << clauses.map { |c| c[0] }.join(" AND ")
            clause += clauses.select{|c| c.length > 1}.map { |c| c[1] }

            clause
          else
            nil
          end
        end

    end
  end
end
