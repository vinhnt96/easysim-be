module Easysim
  module Api
    class Predicate

      attr_reader :pagination
      attr_reader :sorting
      attr_reader :filters
      attr_accessor :includes_tables

      def initialize(pagination:, sorting:, filters:)
        @pagination = pagination
        @sorting = sorting
        @filters = filters
      end

      def apply_on(collection)
        collection
          .includes(includes_tables)
          .where(filters.where_clause)
          .order(sorting.attributes)
          .page(pagination.page)
          .per(pagination.per_page)
      end
    end
  end
end
