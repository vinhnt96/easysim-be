require "google/cloud/storage"

class GoogleCloudStorageService
  GOOGLE_CLOUD_PROJECT_ID = 'bigdongryan-217503'
  GOOGLE_CLOUD_STORAGE_BUCKET = 'easysim_be'

  def initialize params={}
    @bucket = init_bucket 
    @params = params
  end

  def get_matched_files 
    return @bucket.files prefix: "#{@params[:folder_path]}" if @params[:file_name_list].length == 0

    @bucket.files(prefix: "#{@params[:folder_path]}")&.select { |file| @params[:file_name_list].include?(get_basename(file.name)) }
  end

  def read_file
    file_downloaded = @params[:file].download
    unless file_downloaded
      raise APIErrors::ResourceNotFound.new(error_message: 'File not found')
    else
      file_downloaded.read.split("\n")
    end
  end

  private
  def get_basename file_name
    file_name.split("/").last.split(".").first
  end

  def init_bucket
    @storage = Google::Cloud::Storage.new(
      project_id: GOOGLE_CLOUD_PROJECT_ID,
      credentials: "#{Rails.root}/gcloud_credential_key.json"
    )

    @storage.bucket GOOGLE_CLOUD_STORAGE_BUCKET
  end

end