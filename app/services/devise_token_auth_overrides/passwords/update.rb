module DeviseTokenAuthOverrides
  module Passwords
    class Update
      attr_reader :user

      def initialize user
        @user = user
      end

      def excute(params)
        raise APIErrors::ResourceNotFound.new(error_message: "Could not find user") unless user
        password = params.delete(:current_password)
        result = user.valid_password?(password)
        raise APIErrors::ResourceNotFound.new(error_message: "incorrect") unless result
        password = params.fetch(:password)
        result = password.casecmp?(params.fetch(:confirm_password))
        raise APIErrors::ResourceNotFound.new(error_message: "password_not_match") unless result
        user.tokens = {}
        user.update(password: password)
        headers = user.create_new_auth_token
        user.save
        { user: user, headers: headers }
      end
    end
  end
end
