module DeviseTokenAuthOverrides
  module Users
    class InfoCurrentLogin
      attr_reader :user

      def initialize user
        @user = user
      end

      def excute(params)
        device = params[:device]
        browser = params[:browser]
        ip_public = params[:current_ip_v4]
        obj = { online: params.fetch(:online) }
        if ip_public.present?
          results = Geocoder.search(ip_public)
          country_code = results&.first&.country
          obj['country_login'] = country_code
          obj['current_ip_v4'] = ip_public
        end
        obj['device'] = device if device.present?
        obj['browser'] = browser if browser.present?
        obj['current_signin_ip_v4_at'] = DateTime.now if params.fetch(:online)
        if user.current_activity.present? && !params.fetch(:online)
          user.current_activity.update(signout_time: DateTime.current)
        end
        user.update(obj)
        user
      end

    end
  end
end
