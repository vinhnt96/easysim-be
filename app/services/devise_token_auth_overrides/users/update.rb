module DeviseTokenAuthOverrides
  module Users
    class Update
      attr_reader :user

      def initialize user
        @user = user
      end

      def excute(params)
        sim_ids = params.fetch(:sim_ids)
        add = params.fetch(:add)
        sims = user.sim_ids
        return user if user.has_role? :admin
        if sims.blank? && sim_ids.blank?
          sims << user_strategy_selection.id
        else
          if add
            sims << sim_ids if !sims.include?(sim_ids)
          else
            sims =  JSON.parse(sim_ids)
          end
        end
        user.update(sim_ids: sims)
        user
      end

      private

      def user_strategy_selection
        @user_strategy_selection ||= UserStrategySelection.find_by_strategy_selection_id(user.strategy_selection_id)
      end
    end
  end
end
