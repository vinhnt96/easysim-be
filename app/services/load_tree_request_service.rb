class LoadTreeRequestService

  def initialize sims_loaded: [], params: {}
    @sims_loaded = sims_loaded
    @params = params
  end

  def get_sim_name
    positions = @params[:positions]
    positions = positions.split('v').reverse.join('v') unless POSITION_LIST.include?(positions)
    sim_name = "#{@params[:stack_size].to_i}-#{@params[:sim_type]}-#{positions}-#{@params[:flop_cards]}"
    sim_name = "#{sim_name}#{specs}" if @params[:game_type] == 'cash' || (@params[:game_type] == 'mtt' && @params[:stack_size].to_i == 100)

    sim_name
  end

  def specs
    return '-25x8mmtts' if @params[:stack_size].to_i == 100 && (@params[:game_type] == 'mtt' || @params[:specs] == '8max')
    @params[:specs] == 'hu' ? '-25xhus10ks' : '-25x6m500zs'
  end
end
