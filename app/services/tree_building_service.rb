class TreeBuildingService
  def initialize params = {}
    @tree_data = {
      oop: { flop: {}, turn: {}, river: {} },
      ip: { flop: {}, turn: {}, river: {} }
    }
    @params = params
  end

  def get_tree_info
    positions_reverse = @params[:positions].split('v').reverse().join('v')
    file_name = "#{@params[:stack_size]}-#{@params[:sim_type]}-#{@params[:positions]}"
    file_name_reverse = "#{@params[:stack_size]}-#{@params[:sim_type]}-#{positions_reverse}"
    matched_files = GoogleCloudStorageService.new({folder_path: "tree_building_parameter/#{@params[:stack_size]}", file_name_list: [file_name, file_name_reverse]}).get_matched_files
    
    tree_info_file = matched_files.find do |f|
      is_inside_file?( "##{@params[:flop].join}", GoogleCloudStorageService.new({file: f}).read_file )
    end
    raise APIErrors::ResourceNotFound.new(error_message: "Could not find files: #{file_name} or #{file_name_reverse}") unless tree_info_file

    tree_info = GoogleCloudStorageService.new({file: tree_info_file}).read_file
    @params[:lines] = tree_info
    get_tree_data
  end
  
  def get_tree_data
    get_range_ip_oop
    get_additional_info
    get_bet_raise_data
    return @tree_data
  end

  def get_range_ip_oop
    @tree_data[:range_oop] = read_matched_line("set_range OOP", @params[:lines]).strip.split(" ").reject { |r| ["OOP","set_range"].include?(r) }
    @tree_data[:range_ip] = read_matched_line("set_range IP", @params[:lines]).strip.split(" ").reject { |r| ["IP","set_range"].include?(r) }
  end

  def get_additional_info
    @tree_data[:starting_pot] = get_data("#Pot#", @params[:lines])
    @tree_data[:effective_stacks] = get_data("#EffectiveStacks#", @params[:lines])
    @tree_data[:allin_threshold] = get_data("#AllinThreshold#", @params[:lines])
    @tree_data[:add_allin_less_than] = get_data("#AddAllinOnlyIfLessThanThisTimesThePot#", @params[:lines])
    @tree_data[:minimum_betsize] = get_data("#MinimumBetsize#", @params[:lines])
  end

  def get_bet_raise_data
    positions = [{label: "oop", value: ""}, {label: "ip", value: "IP"}]
    rounds = [
      {label: "flop", value: "Flop"},
      {label: "turn", value: "Turn"},
      {label: "river", value: "River"}
    ]
    
    positions.each do |position|
      rounds.each do |round|
        @tree_data[:"#{position[:label]}"][:"#{round[:label]}"][:bet_size] = get_data("#{round[:value]}Config#{position[:value]}.BetSize", @params[:lines])        
        @tree_data[:"#{position[:label]}"][:"#{round[:label]}"][:raise_size] = get_data("#{round[:value]}Config#{position[:value]}.RaiseSize", @params[:lines])
        @tree_data[:"#{position[:label]}"][:"#{round[:label]}"][:add_allin] = get_data("#{round[:value]}Config#{position[:value]}.AddAllin", @params[:lines])

        if position[:label] === "oop" && ["turn","river"].include?(round[:label])
          @tree_data[:"#{position[:label]}"][:"#{round[:label]}"][:donk_bet_size] = get_data("#{round[:value]}Config#{position[:value]}.DonkBetSize", @params[:lines])
        end

      end
    end

  end

  def get_data(sub_string, lines)
    line = read_matched_line(sub_string, lines)
    line = none_data unless line
    line.split("#").last.delete('"').strip
  end

  def read_all_line(file_path)
    file_data = []
    if File.exist?(file_path)
      file_data = File.read(file_path).split("\n") 
    end
  end

  def read_matched_line(sub_string, lines)
    lines.find { |line| line.include?(sub_string) }
  end

  def is_inside_file?(sub_string, lines)
    list_result = lines.select { |line| line.include?(sub_string) }
    list_result.length > 0
  end

  def none_data
    "None"
  end
end