class APIError < StandardError
  R_INTERPOLATION = /\{(?<key>\S*?)\}/
  class_attribute :http_status, :message, :status
  attr_accessor :params

  def initialize(**params)
    self.params = params
  end

  def self.define(status:, message:, http_status: 400)
    ret = Class.new(self).tap do |klass|
      klass.http_status = http_status
      klass.message = message
      klass.status = status
    end
    raise "Error status already defined #{ret.status} for api error, message: #{ret.message.inspect}" if APIError.defined_error_codes.include?(ret.status)

    APIError.all_defined_errors << ret
    ret
  end

  def self.defined_error_codes
    APIError.all_defined_errors.map(&:status)
  end

  def self.all_defined_errors
    @all_defined_errors ||= []
  end

  def self.to_hash
    { status: status, message: message }
  end

  def to_hash
    { status: status, message: message }
  end

  def status
    self.class.status
  end

  def message
    ret = self.class.message
    self.class.message.scan(R_INTERPOLATION) do |match_data|
      key = match_data[0]
      if params.key?(key.to_sym)
        ret = ret.gsub("{#{key}}", params[key.to_sym].to_s)
      else
        raise "Missing value for error interpolation #{key.inspect} for error #{self.class}"
      end
    end
    ret
  end

  def http_status
    self.class.http_status
  end
end
# Force a load of all api errors in case of error code duplication
APIErrors
  