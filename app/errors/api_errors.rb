# All api errors are defined here.
module APIErrors
  TokenExpired = APIError.define(status: 2, message: 'Token has expired')
  Unauthorized = APIError.define(status: 401, message: '{error_message}')
  UnprocessableEntity = APIError.define(status: 4, message: '{error_message}')
  ResourceNotFound = APIError.define(status: 5, message: '{error_message}')
  MissingParameters = APIError.define(status: 6, message: 'Params {params_name} is missing')
  InvalidCredentials = APIError.define(status: 7, message: 'Invalid credentials')
  MissingPaypalAPIAccessToken = APIError.define(status: 8, message: 'Missing Paypal API Access Token')
  ChannelIsBusy = APIError.define(status: 9, message: 'Channel is busy for now to process another request.')
end

