#!/bin/bash
bundle exec rake db:migrate:master
if [ $? -eq 0 ]
then
  bundle exec rake db:migrate:flops
  if [ $? -eq 0 ]
  then
    bundle exec $@
  else
    echo "Failure: migrations flops failed, please check application logs for more details." >&2
    exit 1
  fi
else
  echo "Failure: migrations master failed, please check application logs for more details." >&2
  exit 1
fi