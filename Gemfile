source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1', '>= 6.1.2'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

#Use Devise Invitable to invite user
gem 'devise_invitable', '~> 2.0.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Paginations
gem 'kaminari', '~> 1.2.1'

#rollbar
gem 'rollbar', '~> 3.1.2'

# Provides object geocoding (by street or IP address)
gem 'geocoder', '~> 1.3', '>= 1.3.7'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  # Pry
  gem 'pry'
  gem 'pry-rails'
  gem 'pry-nav'
  gem 'awesome_print'
  gem 'activerecord-import'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'letter_opener', '~> 1.7'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'devise_token_auth', '~> 1.1.5'
gem 'pg', '~> 1.2.3'
gem 'dotenv-rails', '~> 2.7.6'
gem 'rack-cors', '~> 1.1.1'
gem 'roo', '~> 2.8.3'
gem 'appengine', '~> 0.5.0'
gem 'active_model_serializers', '~> 0.10.10'
gem 'httparty', '~> 0.18.1'
gem 'rolify', '~> 5.3.0'
gem 'slim', '~> 4.1.0'
gem 'retryable', '~> 3.0.5'
gem 'google-cloud-storage', '~> 1.29.2'
gem 'sidekiq', '~> 6.1'
gem 'redis-rails', '~> 5.0.2'
gem 'sidekiq_alive', '~> 2.0'
gem 'devise-i18n', '~> 1.9.3'
gem 'foreman', '~> 0.87.2'
gem 'paymentwall', '~> 1.1', '>= 1.1.1'
