# README

**Ruby version**

- ruby-2.6.3

**Rails version**

- Rails 5.0.7

**Postgresql version**

- PostgreSQL 12

**Background job**

- Sidekiq

## HOW TO START APP ON YOUR LOCAL

- Create `.env` base on `.env.example`
- Create your `gcloud_credential_key.json`
    (https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account)
- Run `bundle`

***Database setup***

- Create `easysim_development` database by using Postgresql console. (`rake db:create` will not work)
- Run migration: `rake db:migrate`
- Download data files: `stacksizes.csv`, `flops_9s7d3c.csv` from Google Drive and save them into project folder.
- Import data:
    ```
    rake import:type_options_mapping_data
    rake import:preflop_data 
    rake import:data_stacksizes
    rake import:data_flops_9s7d3c 
    rake db:seed
    ```

***User accounts***

- Admin user:
```
Email: odinpoker_admin@yopmail.com
Password: Password123
```
- Pokercode user:
```
Email: pokercode_user@yopmail.com
Password: Password123
```
- Regular user:
```
Email: odinpoker_user@yopmail.com
Password: Password123
```

**Start backend server**

- Run `rails s -p 4000`

**Run background job**

- Install [redis] (https://redis.io/topics/quickstart)

- In root directory, run `bundle exec sidekiq -q default -q mailers` to start sidekiq

## DEPLOYMENT

**Preparing for deployment**

- Download `gcloud SDK` and make authorize.
- Create `production_env_variables.yaml` OR `staging_env_variables.yaml` base on `env_variables.example.yaml`

**How to deploy staging**

- Ensure you checked-out to `staging`.
- Ensure you created `staging_env_variables.yaml`.
- Merge your branch into `staging`.
- Run `gcloud app deploy staging.yaml` to deploy.

**How to deploy production**

- Ensure you checked-out to `master`.
- Ensure you created `production_env_variables.yaml`.
- Merge your branch into `master`.
- Run `gcloud app deploy` to deploy.

**Access Auth**

- For Staging:
```
username: staging
password: Diamonds123
```

- For Production:
```
username: odinpoker_admin
password: Clubs888
```
