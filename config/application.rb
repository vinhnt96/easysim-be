require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module EasySim
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.middleware.use Rack::Cors do
      allow do
        origins 'https://odinpoker.io', 'https://www.odinpoker.io', 'https://staging.odinpoker.io', 'localhost:3000'
        resource '*',
          headers: :any,
          expose: [
            'access-token',
            'expiry',
            'token-type',
            'uid',
            'client',
            'X-Pagination-Page',
            'X-Pagination-Total-Records',
            'X-Pagination-Total-Pages',
            'X-Pagination-Per-Page',
            'X-Pagination-Previous-Page',
            'X-Pagination-Next-Page',
            'X-Sorting',
             # Legacy
            'X-Pagination' ],
          methods: [:get, :post, :options, :delete, :put]
      end
    end

    config.active_job.queue_adapter = :sidekiq
    config.i18n.default_locale = :en
    # config.hosts = nil
  end
end
