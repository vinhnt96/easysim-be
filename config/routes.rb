Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  root to: "rails/welcome#index"
  mount_devise_token_auth_for 'User',
  at: 'auth',
  controllers: {
    sessions:  'devise_token_auth_overrides/sessions',
    registrations:  'devise_token_auth_overrides/registrations',
    passwords:  'devise_token_auth_overrides/passwords',
    confirmations:  'devise_token_auth_overrides/confirmations',
    invitations: 'devise_token_auth_overrides/invitations'
  }

  devise_scope :user do
    get 'invitation_token', to: "devise_token_auth_overrides/invitations#invitation_token"
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :type_options_mappings, only: %i[] do
    collection do
      get :get_type_options
    end
  end
  resources :user_preferences, only: [:update]
  resources :strategy_selections, only: %i[create] do
    collection do
      get :get_strategy_selection
    end
  end

  namespace :admin do
    namespace :paypal do
      resources :plans, only: [:index, :create]
    end
    resources :users, only: [:index, :update, :destroy] do
      member do
        post :resent_confirm_email
      end
    end
    resources :subscriptions, only: [:index]
    resources :activities, only: [:index]
  end

  resources :users, only: [:update, :show] do
    member do
      put :update_info_current_login
    end
    collection do
      post :check_exists
    end
  end
  resources :roles, only: [:index]
  resources :plans, only: [:index]
  resources :subscriptions, only: [:create, :index] do
    collection do
      post :cancel_current_subscription
      post :change_subscription
    end
  end
  resources :fastspring_webhooks, only: [] do
    collection do
      post :order_completed
      post :subscription_canceled
      post :order_failed
      post :subscription_deactivated
      post :subscription_charge_completed
      post :subscription_charge_failed
      post :subscription_updated
    end
  end
  resources :bug_reports, only: [:create]
  resources :contact_us_messages, only: [:create]
  resources :paymentwalls, only: [] do
    collection do
      post :widget_call
      get :pingback_process
    end
  end
  resources :stacksizes, only: [] do
    collection do
      post :get_initial_stacksize
    end
  end
  resource :load_tree, only: [:create] do
    collection do
      get :existing_channels
    end
  end
  resources :my_sims, only: [:index, :update] do
    collection do
      post :delete_sims
    end
  end
  resources :nodes, only: [] do
    collection do
      post :more_details_data
      post :next_flops_details_data
      post :get_nodes
      post :show_current_node
      post :get_turn_river_data
      get :free_tree
      post :get_runouts_explorer_data
    end
  end
  resources :preflops, only: [] do
    collection do
      get :get_preflop_data
    end
  end
  resources :subscribers, only: [:create]
  resources :tree_building_parameter, only: [] do
    collection do
      post :get_tree_info
    end
  end
  resources :universal_notes, only: [:show, :update]
  resources :hand_categories, only: [] do
    collection do
      get :get_hand_category
    end
  end
end
