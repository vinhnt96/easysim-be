SidekiqAlive.setup do |config|
  # ==> Server port
  # Port to bind the server.
  config.port = ENV['PORT']

  # ==> Server path
  # HTTP path to respond to.
  config.path = '/health_check'

  # ==> Rack server
  # Web server used to serve an HTTP response.
  config.server = 'puma'
end