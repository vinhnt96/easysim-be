PER_PAGE = 20
MAX_PER_PAGE = 100
LIMIT_SIMS = 100
POSITION_LIST = [
  "B+1vB+2","B+1vB+3","B+1vB+4","B+1vB+5","B+2vB+3","B+2vB+4","B+2vB+5","B+3vB+4",
  "B+3vB+5","B+4vB+5","BBvB","BBvB+1","BBvB+2","BBvB+3","BBvB+4","BBvB+5","BBvSB",
  "BvB+1","BvB+2","BvB+3","BvB+4","BvB+5","SBvB","SBvB+1","SBvB+2","SBvB+3",
  "SBvB+4","SBvB+5",
]

CONVERT_FLOPS_MAP = {
  "2" => "01",
  "3" => "02",
  "4" => "03",
  "5" => "04",
  "6" => "05",
  "7" => "06",
  "8" => "07",
  "9" => "08",
  "10" => "09",
  "j" => "10",
  "q" => "11",
  "k" => "12",
  "a" => "13"
}

SIM_TYPES_MAPPING = {
  'limped' => 'limp',
  '3bet' => '3bp',
  '4bet' => '4bp',
  'l3bet' => 'l3b',
  'l4bet' => 'l4b',
  'iso' => 'liso'
}

CODE_DISCOUNT_TO_ENUM_MAPPING = {
  "PC45" => "pokercode",
  "GGPLATINUM" => "ggplatinum",
  "GINGE" => "ginge",
  "ELLIOT" => "elliot",
  "BTS" => "bts",
  "STREAMTEAM" => "streamteam",
  "CAVALITOPOKER" => "cavalitopoker",
  "POKERCODE" => "pc",
  "KMART" => "kmart",
  "PC50" => "pc50",
  "STANDARD" => "standard",
  "PADS35" => "pads",
  "PAV35" => "pav",
  "JNANDEZ" => "jnandez",
  "REGLIFE" => "reglife"
}

HUNL_CASH_GAME_STRATEGY = {
  "street" => "postflop",
  "game_type" => "cash",
  "stack" => "100",
  "positions" => ["bb", "b"],
  "sim_type" => "3bet",
  "site" => "",
  "stake" => "",
  "open_size" => "",
  "flops" => ["Js", "8d", "7d"],
  "specs" => "hu"
}

SIX_MAX_CASH_GAME_STRATEGY = {
  "flops" => ["Ad", "Kd", "5s"],
  "game_type" => "cash",
  "open_size" => "",
  "positions" => ["sb", "b"],
  "sim_type" => "3bet",
  "site" => "",
  "specs" => "6max",
  "stack" => "100",
  "stake" => "",
  "street" => "postflop"
}

MTT_GAME_STRATEGY = {
  "flops" => ["6s", "4d", "3s"],
  "game_type" => "mtt",
  "open_size" => "",
  "positions" => ["b", "bb"],
  "sim_type" => "srp",
  "site" => "",
  "specs" => "",
  "stack" => "25",
  "stake" => "",
  "street" => "postflop"
}
