namespace :strategy_selection do
  task update_specs_value: :environment do
    StrategySelection.where(specs: '25x6m500zs').update_all(specs: '6max')
  end
end
    