namespace :subscriptions do
  task update_fastspring_subscription_id: :environment do
    fastspring_auth ||= Fastspring::BaseService.get_credentials
    scope = Rails.env == 'production' ? 'live' : 'test'
    res = Fastspring::SubscriptionService.new({limit: 200, scope: scope}, fastspring_auth).get_all
    fastspring_subs_ary = res["subscriptions"]

    Subscription.all.each do |sub|
      start_time = sub.start_time.utc
      fspr_sub = fastspring_subs_ary.select do |fastspring_sub|
        begin_time = Time.at(fastspring_sub["changed"].to_i/1000).utc
        DateTime.new(start_time.year, start_time.month, start_time.day, start_time.hour, start_time.min, 0, 'utc') == DateTime.new(begin_time.year, begin_time.month, begin_time.day, begin_time.hour, begin_time.min, 0, 'utc')
      end

      unless fspr_sub.blank?
        fastspring_subscription_id = fspr_sub[0]["id"]
        sub.update(fastspring_subscription_id: fastspring_subscription_id)
      end
    end
  end

  task update_subscription_additional_info: :environment do
    fastspring_auth ||= Fastspring::BaseService.get_credentials
    Subscription.all.find_each do |sub|
      plan = Plan.find(sub[:plan_id])
      num_of_month = plan.billing_period == 'MONTH' ? 1 : 12
      cycle_time = plan.billing_frequency * num_of_month
      renew_time = plan.billing_period == 'MONTH' ? sub[:start_time] + plan.billing_frequency.months : sub[:start_time] + plan.billing_frequency.years
      if sub[:fastspring_subscription_id]
        fastspring_sub = Fastspring::SubscriptionService.new({fastspring_subscription_id: sub[:fastspring_subscription_id]}, fastspring_auth).get
        paid = ['canceled', 'deactivated'].include?(fastspring_sub["state"]) ? false : true 
        sub.update(cycle_time: cycle_time, renew_time: renew_time, auto_renew: fastspring_sub["autoRenew"], status: fastspring_sub["state"].to_sym, paid: paid)
      else
        sub.update(cycle_time: cycle_time, renew_time: renew_time, auto_renew: false, status: :deactivated, paid: false)
      end
    end
  end

  task update_subscription_code_discount: :environment do
    fastspring_auth ||= Fastspring::BaseService.get_credentials
    Subscription.all.find_each do |sub|
      if sub[:fastspring_subscription_id]
        fastspring_sub = Fastspring::SubscriptionService.new({fastspring_subscription_id: sub[:fastspring_subscription_id]}, fastspring_auth).get
        discount = fastspring_sub["discounts"]
        code_discount = discount.present? ? convert_code_discount(discount[0]["discountPath"]) : ""
        sub.update(code_discount: code_discount)
      else
        sub.update(code_discount: '')
      end
    end
  end

  task upgrade_subscription_for_special_user: :environment do
    sub_params = { 
      plan_id: Plan.find_by(product_id: "cash-mtts-yearly")[:id],
      start_time: Time.current,
      paid: true,
      status: "active",
      auto_renew: false,
      cycle_time: 12,
      renew_time: Time.current + 1.year
    }
    u = User.find_by(email: "chiconogue@gmail.com")
    if u.current_subscription.present?
      u.current_subscription.update(sub_params)
    else
      u.subscriptions.create(sub_params)
    end
  end

  private
  def convert_code_discount(discount_path)
    case discount_path
      when "Pokercode-45-off" 
        "PC45"
      when "GG-Platinum-Members"
        "GGPLATINUM"
      when "Ginge"
        "GINGE"
      when "Elliot"
        "ELLIOT"
      when "BTS"
        "BTS"
      when "STREAM-TEAM"
        "STREAMTEAM"
      when "CAVALITOPOKER"
        "CAVALITOPOKER"
      when "pokercode-public"
        "POKERCODE"
      when "KMART"
        "KMART"
      when "ODIN-LAUNCH"
        "LAUNCH35"
      when "pokercode-discount"
        "PC50"
      else 
        ""
      end
  end

end
