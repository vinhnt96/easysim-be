namespace :paypal_subscription do
  desc "Import plans into local DB"
  task import_active_plans: :environment do
    @paypal_api_access_token = GetPaypalApiAccessTokenService.call
    body = JSON.parse Admin::Paypal::PlanService.new(@paypal_api_access_token).get_list
    plans = body['plans'] || []
  
    plans.filter{ |plan| plan['status'] == 'ACTIVE'}.each do |pl|
      plan = JSON.parse Admin::Paypal::PlanService.new(@paypal_api_access_token, {plan_id: pl['id']}).get_details
      Plan.find_or_create_by(paypal_plan_id: plan['id']) do |new_plan|
        new_plan.paypal_product_id = plan['product_id']
        new_plan.name = plan['name']
        new_plan.description = plan['description']
        new_plan.paypal_status = plan['status']
        new_plan.price = plan['billing_cycles'][0]['pricing_scheme']['fixed_price']['value'].to_f
        new_plan.currency = plan['billing_cycles'][0]['pricing_scheme']['fixed_price']['currency_code']
        new_plan.billing_period = plan['billing_cycles'][0]['frequency']['interval_unit']
        new_plan.billing_frequency = plan['billing_cycles'][0]['frequency']['interval_count']
      end
    end

  end

end