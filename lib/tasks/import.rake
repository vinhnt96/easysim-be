namespace :import do
  desc "Import data from spreadsheet"
  task type_options_mapping_data: :environment do
    puts 'Importing Data'

    data = Roo::Spreadsheet.open('lib/type-options-mapping.ods') # open spreadsheet
    headers = data.row(1) # get header row
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      data.each_with_index do |row, idx|
        next if idx == 0 # skip header row

        type_options_mapping_data = Hash[[headers, row].transpose]
        type_options_mapping_data['stack'] = type_options_mapping_data['stack'].to_s
        type_options_mapping_data['positions'] = [
          type_options_mapping_data['position_1'],
          type_options_mapping_data['position_2']
        ].compact.sort
        type_options_mapping_data['type_options'] = type_options_mapping_data['type_options']&.split(',') || []
        type_options_mapping_data.except!('position_1', 'position_2')

        mapping = TypeOptionsMapping.new(type_options_mapping_data)
        mapping.type_options.map! { |option| option.strip } if mapping.type_options.present?
        mapping.save!
      end
    end
  end

  task preflop_data: :environment do
    puts 'Importing Preflop Data'

    data = Roo::Spreadsheet.open('lib/preflop_mmt_30.ods')
    headers = data.row(1) # get header row
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      data.each_with_index do |row, idx|
        next if idx == 0 # skip header row

        preflop = Hash[[headers, row].transpose]

        record_existing = Preflop.where(
          rake: preflop['rake'],
          icm: preflop['icm'],
          game_type: preflop['game_type'],
          stack_size: preflop['stack_size'],
          node: preflop['node'],
          actions: preflop['actions'],
          ranges: preflop['ranges']
        ).exists?

        next if record_existing

        Preflop.create!(preflop)
      end
    end
  end

  task data_flops_9s7d3c: :environment do
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      BATCH_SIZE = 1_000
      flops = []
      CSV.foreach("flops_9s7d3c.csv", headers: true) do |row|
        data = {
          game_type: row['game_type'],
          stack_size: row['stack_size'],
          sim_type: row['sim_type'],
          flop_cards: row['flop_cards'],
          positions: row['positions'],
          nodes: row['nodes'],
          node_count: row['node_count'],
          pot_ip: row['pot_ip'],
          pot_oop: row['pot_oop'],
          pot_main: row['pot_main'],
          strategy: row['strategy'],
          num_combos_oop: row['num_combos_oop'],
          num_combos_ip: row['num_combos_ip'],
          ranges_oop: row['ranges_oop'],
          ranges_ip: row['ranges_ip'],
          ev_oop: row['ev_oop'],
          ev_ip: row['ev_ip'],
          equity_oop: row['equity_oop'],
          equity_ip: row['equity_ip'],
          probs_oop: row['probs_oop'],
          probs_ip: row['probs_ip'],
          probs: row['probs'],
          eq_oop2: row['eq_oop2'],
          ev_oop2: row['ev_oop2'],
          eq_ip2: row['eq_ip2'],
          ev_ip2: row['ev_ip2'],
          eq_node_oop: row['eq_node_oop'],
          ev_node_oop: row['ev_node_oop'],
          eq_node_ip: row['eq_node_ip'],
          ev_node_ip: row['ev_node_ip'],
          global_freq: row['global_freq'],
          specs: row['specs']
        }
        flops << Flop.new(data)
      end
      flops.in_groups_of(BATCH_SIZE, false).each do |batch|
        Flop.import! batch, on_duplicate_key_update: {
          conflict_target: [:id],
          columns: %i[
            game_type
            stack_size
            sim_type
            flop_cards
            positions
            nodes
            node_count
            pot_ip
            pot_oop
            pot_main
            strategy
            num_combos_oop
            num_combos_ip
            ranges_oop
            ranges_ip
            ev_oop
            ev_ip
            equity_oop
            equity_ip
            probs_oop
            probs_ip
            probs
            eq_oop2
            ev_oop2
            eq_ip2
            ev_ip2
            eq_node_oop
            ev_node_oop
            eq_node_ip
            ev_node_ip
            global_freq
            specs
          ]
        }
        puts "Imported #{flops.map(&:id)}"
      end

    end
  end

  task data_stacksizes: :environment do
    stacksizes = []
    path_name = "stacksizes.csv"
    file_length = CSV.read(path_name).count
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      CSV.foreach(path_name, headers: true).with_index do |row, i|
        data = {
          filename: row['filename'],
          initial_stacksize: row['initial_stacksize']
        }
        stacksizes << Stacksize.new(data)
        
        if stacksizes.size > 1000 || i == file_length-2
          Stacksize.import! stacksizes, on_duplicate_key_update: {
            conflict_target: [:id],
            columns: %i[
              filename
              initial_stacksize
            ]
          }
          puts "Imported #{stacksizes.map(&:id)}"
          stacksizes = []
        end
      end
    end
  end

  # This rake only use for import local hand_categories data
  task hand_categories_data: :environment do
    ActiveRecord::Base.connected_to(role: :reading, shard: :flops) do
      BATCH_SIZE = 1_000
      hands = []
      CSV.foreach("hand_categories.csv", headers: true) do |row|
        data = {
          board: row['board'],
          made_hands: row['made_hands'],
          draw_hands: row['draw_hands']
        }
        hands << HandCategory.new(data)
      end
      hands.in_groups_of(BATCH_SIZE, false).each do |batch|
        HandCategory.import! batch, on_duplicate_key_update: {
          conflict_target: [:id],
          columns: %i[
            board
            made_hands
            draw_hands
          ]
        }
        puts "Imported #{hands.map(&:id)}"
      end

    end
  end

end
