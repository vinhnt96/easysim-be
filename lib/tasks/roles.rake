namespace :roles do
  task update_title: :environment do
    Role.all.find_each do |role|
      role.update(title: 'Admin') if role.name.casecmp? 'admin'
      role.update(title: 'Odin user') if role.name.casecmp? 'odin_user'
      role.update(title: 'PC design') if role.name.casecmp? 'pokercode_user'
    end
  end
end
  