namespace :update_user do
  task update_poker_user_role_for_staging: :environment do
    user = User.find_by(email: 'pokercodeuserdemo@gmail.com');

    if user.present?
      user.add_role(:pokercode_user)
      user.remove_role(:odin_user)
    end
  end

  task update_admin_account_for_staging: :environment do
    admins = [
      'heimdallrpoker@hotmail.com',
      'Ryananthonyotto@gmail.com',
      'roryyoungpoker@gmail.com',
      'odin_admin@yopmail.com',
      'kinz@odinpoker.io'
    ]
    users = User.where(email: admins)
    
    users.each do |user|
      if user.present?
        user.add_role(:admin)
        user.remove_role(:pokercode_user)
        user.remove_role(:odin_user)
      end
    end
  end

  task update_confirmed_at_for_all_user: :environment do  # NOTE: this rake task work only on master
    User.where("confirmed_at IS NULL").each do |user|
      user.confirm
    end
  end

  task set_roryyoungpoker_to_admin_on_production: :environment do
    admin_user = User.find_by email: 'roryyoungpoker@gmail.com'

    admin_user.update(user_type: :paid) if admin_user.present?
    admin_user.add_role(:admin) if admin_user.present?
  end

  task accessable_game_type: :environment do
    User.all.each do |user|
      user.update(accessable_game_type: 'cash-mtts')
    end
  end

  task user_type: :environment do
    User.where(user_type: :paid).each do |user|
      current_subscription = user.current_subscription
      if current_subscription
        if !current_subscription.active? || !current_subscription.canceled?
          user.update(user_type: :free)
        end
      else
        user.update(user_type: :free)
      end
    end
  end
  
  task update_current_subscription_id: :environment do
    User.all.find_each do |user|
      current_subscription = user.subscriptions.where(paid: true).order(start_time: :desc).first
      user.update(current_subscription_id: current_subscription[:id]) if current_subscription.present?
    end
  end

  task affiliate_code_from_gg_to_ggplatinum: :environment do
    User.where(affiliate_code: 'gg').each do |user|
      user.update(affiliate_code: 'ggplatinum')
    end
  end

end
