namespace :init_stacksizes do
  task fix_filename_for_cash_game: :environment do
    ActiveRecord::Base.connected_to(role: :writing, shard: :flops) do
      stacksizes = Stacksize.where("filename ILIKE ? AND filename NOT ILIKE ?", "100-%", "%-25xhus10ks")
      stacksizes.each do |stacksize|
        prefix = stacksize.filename.gsub("-25x", "")
        stacksize.update(filename: "#{prefix}-25x6m500zs")
      end
    end
  end
end