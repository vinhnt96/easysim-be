namespace :plans do
  task add_new_plans: :environment do
    old_plans = Plan.where(parent_code: nil)
    old_plans.destroy_all
    attributes = [
      {price: 153, name: 'CASH', product_id: 'cash', parent: true, parent_code: 'cash', currency: 'USD', billing_frequency: 1, billing_period: 'MONTH'},
      {price: 414, name: 'CASH', product_id: 'cash-3-monthly', parent: false, parent_code: 'cash', currency: 'USD', billing_frequency: 3, billing_period: 'MONTH'},
      {price: 1380, name: 'CASH', product_id: 'cash-yearly', parent: false, parent_code: 'cash', currency: 'USD', billing_frequency: 1, billing_period: 'YEAR'},
      {price: 299, name: 'ULTIMATE', product_id: 'cash-mtts', parent: true, parent_code: 'cash-mtts', currency: 'USD', billing_frequency: 1, billing_period: 'MONTH'},
      {price: 807, name: 'ULTIMATE', product_id: 'cash-mtts-3-monthly', parent: false,parent_code: 'cash-mtts', currency: 'USD', billing_frequency: 3, billing_period: 'MONTH'},
      {price: 2688, name: 'ULTIMATE', product_id: 'cash-mtts-yearly', parent: false, parent_code: 'cash-mtts', currency: 'USD', billing_frequency: 1, billing_period: 'YEAR'},
      {price: 249, name: 'MTTs', product_id: 'mtts', parent: true,parent_code: 'mtts', currency: 'USD', billing_frequency: 1, billing_period: 'MONTH'},
      {price: 672, name: 'MTTs', product_id: 'mtts-3-monthly', parent: false,parent_code: 'mtts', currency: 'USD', billing_frequency: 3, billing_period: 'MONTH'},
      {price: 2244, name: 'MTTs', product_id: 'mtts-yearly', parent: false,parent_code: 'mtts', currency: 'USD', billing_frequency: 1, billing_period: 'YEAR'}
    ]
    attributes.each do |params|
      old_plan = Plan.find_by_product_id(params[:product_id])
      if old_plan.present?
        Plan.update(params)
      else
        Plan.create(params)
      end
    end
  end
end
