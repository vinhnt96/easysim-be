namespace :sim_type_options do
  task add_more_sim_type_options: :environment do
    data = [   
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'b1', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'b2', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'b3', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'b4', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'b', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'sb', position_2: 'b1', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'sb', position_2: 'b2', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'sb', position_2: 'b3', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'sb', position_2: 'b4', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'sb', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'sb', position_2: 'b', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b', position_2: 'b1', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b', position_2: 'b2', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b', position_2: 'b3', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b', position_2: 'b4', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b1', position_2: 'b2', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b1', position_2: 'b3', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b1', position_2: 'b4', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b1', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b2', position_2: 'b3', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b2', position_2: 'b4', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b2', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b3', position_2: 'b4', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b3', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'b4', position_2: 'b5', type_options: ['srp', '3bp', '4bp'], players: '8max' },
      { game_type: 'cash', stack: 100, position_1: 'bb', position_2: 'sb', type_options: ['srp', '3bp', '4bp', 'l3bet', 'l4bet', 'limped', 'iso'], players: '8max' },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'b1', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'b2', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'b3', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'b4', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'b', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'sb', position_2: 'b1', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'sb', position_2: 'b2', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'sb', position_2: 'b3', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'sb', position_2: 'b4', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'sb', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'sb', position_2: 'b', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b', position_2: 'b1', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b', position_2: 'b2', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b', position_2: 'b3', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b', position_2: 'b4', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b1', position_2: 'b2', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b1', position_2: 'b3', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b1', position_2: 'b4', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b1', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b2', position_2: 'b3', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b2', position_2: 'b4', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b2', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b3', position_2: 'b4', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b3', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'b4', position_2: 'b5', type_options: ['srp', '3bp', '4bp'] },
      { game_type: 'mtt', stack: 100, position_1: 'bb', position_2: 'sb', type_options: ['srp', '3bp', '4bp', 'l3bet', 'l4bet', 'limped', 'iso'] }
    ]

    ActiveRecord::Base.connected_to(role: :writing, shard: :flops) do
      data.each do |row|
        type_options_mapping_data = row
        type_options_mapping_data[:stack] = row[:stack].to_s
        type_options_mapping_data[:positions] = [
          row[:position_1],
          row[:position_2]
        ].compact.sort
        type_options_mapping_data[:specs] = row[:players]
        type_options_mapping_data.except!(:position_1, :position_2)
        type_options_mapping_data.delete(:players)

        mapping = TypeOptionsMapping.new(type_options_mapping_data)
        mapping.type_options.map! { |option| option.strip } if mapping.type_options.present?
        mapping.save!
      end
    end
  end
end
  