# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_19_083623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.datetime "logged_time"
    t.datetime "signout_time"
    t.datetime "begin_date"
    t.integer "sim_ids", default: [], array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["logged_time", "sim_ids"], name: "index_activities_on_logged_time_and_sim_ids"
    t.index ["user_id"], name: "index_activities_on_user_id"
  end

  create_table "bug_reports", id: :serial, force: :cascade do |t|
    t.string "bug_type"
    t.string "subject"
    t.string "description"
    t.string "page"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_bug_reports_on_user_id"
  end

  create_table "contact_us_messages", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flops", id: :serial, force: :cascade do |t|
    t.string "game_type"
    t.string "stack_size"
    t.string "sim_type"
    t.string "flop_cards"
    t.string "positions"
    t.string "nodes"
    t.integer "node_count"
    t.integer "pot_ip"
    t.integer "pot_oop"
    t.integer "pot_main"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "strategy"
    t.float "num_combos_oop"
    t.float "num_combos_ip"
    t.text "ranges_oop"
    t.text "ranges_ip"
    t.text "ev_oop"
    t.text "ev_ip"
    t.text "equity_oop"
    t.text "equity_ip"
    t.float "probs_oop"
    t.float "probs_ip"
    t.float "probs"
    t.text "eq_oop2"
    t.text "ev_oop2"
    t.text "eq_ip2"
    t.text "ev_ip2"
    t.float "eq_node_oop"
    t.float "ev_node_oop"
    t.float "eq_node_ip"
    t.float "ev_node_ip"
    t.float "global_freq"
    t.string "specs"
  end

  create_table "hand_categories", id: :serial, force: :cascade do |t|
    t.string "board"
    t.string "made_hands"
    t.string "draw_hands"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board"], name: "unique_board", unique: true
  end

  create_table "ip_histories", id: :serial, force: :cascade do |t|
    t.inet "ip"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_ip_histories_on_user_id"
  end

  create_table "plans", id: :serial, force: :cascade do |t|
    t.string "product_id"
    t.string "name"
    t.string "description"
    t.float "price"
    t.string "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "billing_period"
    t.integer "billing_frequency"
    t.boolean "parent", default: false
    t.string "parent_code"
  end

  create_table "preflops", id: :serial, force: :cascade do |t|
    t.string "rake"
    t.string "icm"
    t.string "game_type"
    t.string "stack_size"
    t.string "node"
    t.string "actions"
    t.string "ranges"
    t.index ["game_type", "stack_size", "node"], name: "preflops_game_type_stack_size_node_idx"
  end

  create_table "roles", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.integer "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "stacksizes", id: :serial, force: :cascade do |t|
    t.string "filename"
    t.float "initial_stacksize"
    t.index ["filename"], name: "unique_filename", unique: true
  end

  create_table "strategy_selections", id: :serial, force: :cascade do |t|
    t.string "game_type"
    t.float "stack"
    t.string "positions", default: [], array: true
    t.string "sim_type"
    t.string "flops", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "street", default: "postflop"
    t.string "site"
    t.string "stake"
    t.string "open_size"
    t.string "strategy_selection_type", default: "paid"
    t.string "flops_key"
    t.string "positions_key"
    t.string "specs"
    t.index ["flops_key"], name: "index_on_flops_key"
    t.index ["positions_key"], name: "index_on_positions_key"
  end

  create_table "subscribers", id: :serial, force: :cascade do |t|
    t.string "email"
  end

  create_table "subscriptions", id: :serial, force: :cascade do |t|
    t.integer "plan_id"
    t.integer "user_id"
    t.datetime "start_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code_discount"
    t.boolean "paid", default: false
    t.string "fastspring_subscription_id"
    t.integer "status", default: 0
    t.boolean "auto_renew", default: true
    t.float "cycle_time"
    t.datetime "renew_time"
    t.index ["plan_id"], name: "index_subscriptions_on_plan_id"
    t.index ["user_id"], name: "index_subscriptions_on_user_id"
  end

  create_table "type_options_mappings", id: :serial, force: :cascade do |t|
    t.string "game_type"
    t.string "stack"
    t.string "positions", default: [], array: true
    t.string "type_options", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "specs"
    t.index ["game_type", "stack", "positions"], name: "index_on_game_type_and_stack_and_positions"
  end

  create_table "universal_notes", id: :serial, force: :cascade do |t|
    t.text "note_data"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_universal_notes_on_user_id"
  end

  create_table "user_preferences", id: :serial, force: :cascade do |t|
    t.string "fold_color", default: "blue"
    t.string "check_and_call_color", default: "green"
    t.string "bet_and_raise_color", default: "red"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "round_strategies_to_closest", default: 0.0
    t.float "hide_strategies_less_than", default: 0.01
    t.boolean "hide_explanation", default: false
    t.boolean "display_positions_by_default", default: true
    t.index ["user_id"], name: "index_user_preferences_on_user_id"
  end

  create_table "user_strategy_selections", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "strategy_selection_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "notes"
    t.datetime "date_last_view"
    t.boolean "is_noted", default: false
    t.boolean "active", default: true
    t.index ["user_id", "strategy_selection_id"], name: "index_on_user_id_and_strategy_selection_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "first_name"
    t.string "last_name"
    t.date "birthday"
    t.string "country"
    t.integer "strategy_selection_id"
    t.string "api_key"
    t.string "user_type", default: "free"
    t.integer "sim_ids", default: [], array: true
    t.string "current_ip_v4"
    t.datetime "current_signin_ip_v4_at"
    t.string "device"
    t.string "browser"
    t.boolean "online"
    t.string "country_login"
    t.integer "current_subscription_id"
    t.string "affiliate_code"
    t.string "accessibility_game_type"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["strategy_selection_id"], name: "index_users_on_strategy_selection_id"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
  end

end
