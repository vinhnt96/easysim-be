# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Create roles
puts '===== CREATING ROLES ====='
roles = [ {name: 'admin', title: 'Admin'},
roles = [ {name: 'free_user', title: 'Free User'},
          {name: 'pokercode_user', title: 'PC design'},
          {name: 'odin_user', title: 'Odin user'} ]

roles.each do |role| 
  Role.find_or_create_by(name: role[:name]) do |r|
    r.title = role[:title]
  end
end
puts '===== DONE ====='

# Create user
puts '===== CREATING USERS ====='

user = {
  first_name: 'odinpoker',
  last_name: 'user',
  email: 'odinpoker_user@yopmail.com',
  password: 'Password123',
  country: 'Argentina',
}
new_user = User.create(user)
new_user.confirm

pokercode_user = {
  first_name: 'pokercode',
  last_name: 'user',
  email: 'pokercode_user@yopmail.com',
  password: 'Password123',
  country: 'Argentina',
}
new_user = User.create(pokercode_user)
new_user.confirm
new_user.remove_role :odin_user
new_user.add_role :pokercode_user

puts '===== DONE ====='

# Create admin user
puts '===== CREATING ADMIN ====='
admin_user = {
  first_name: 'odinpoker',
  last_name: 'admin',
  email: 'odinpoker_admin@yopmail.com',
  password: 'Password123',
  country: 'Argentina',
}
admin = User.create(admin_user)
admin.confirm
admin.remove_role :odin_user
admin.add_role :admin
puts '===== DONE ====='
