class CreateIpHistory < ActiveRecord::Migration[6.0]
  def up
    create_table :ip_histories do |t|
      t.inet :ip
      t.references :user
      t.timestamps
    end
    remove_column :users, :ips
  end

  def down
    add_column :users, :ips, :inet, array: true, default: []
    drop_table :ip_histories
  end
end
