class FixIndexOnPreflops < ActiveRecord::Migration[6.0]
  def up
    remove_index :preflops, name: "index_preflops_on_game_type_and_stack_size_and_node" if index_exists?(:preflops, [:game_type, :stack_size, :node], name: "index_preflops_on_game_type_and_stack_size_and_node")
    add_index :preflops, [:game_type, :stack_size, :node], name: "preflops_game_type_stack_size_node_idx" unless index_exists?(:preflops, [:game_type, :stack_size, :node], name: "preflops_game_type_stack_size_node_idx")
  end

  def down
    remove_index :preflops, name: "preflops_game_type_stack_size_node_idx" if index_exists?(:preflops, [:game_type, :stack_size, :node], name: "preflops_game_type_stack_size_node_idx")
    add_index :preflops, [:game_type, :stack_size, :node]
  end
end
