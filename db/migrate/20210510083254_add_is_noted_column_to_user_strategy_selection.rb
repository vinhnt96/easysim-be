class AddIsNotedColumnToUserStrategySelection < ActiveRecord::Migration[6.0]
  def up
    add_column :user_strategy_selections, :is_noted, :boolean, default: false
    
    UserStrategySelection.find_each do |user_strategy|
      user_strategy.update(notes: nil, is_noted: false)
    end
  end
  def down
    remove_column :user_strategy_selections, :is_noted
  end
end
