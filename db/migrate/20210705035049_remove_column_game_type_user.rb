class RemoveColumnGameTypeUser < ActiveRecord::Migration[6.1]
  def up
    remove_column :users, :accessable_game_type, :string
  end

  def down
    add_column :users, :accessable_game_type, :string
  end
end
