class AddUserTypeToUsers < ActiveRecord::Migration[6.0]
  def up
    add_column :users, :user_type, :string, default: :free

    User.all.find_each do |user|
      user.update(user_type: :paid)
    end
  end

  def down
    remove_column :users, :user_type
  end
end