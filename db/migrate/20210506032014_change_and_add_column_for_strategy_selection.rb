class ChangeAndAddColumnForStrategySelection < ActiveRecord::Migration[6.0]
  def up
    add_column :strategy_selections, :flops_key, :string
    add_column :strategy_selections, :positions_key, :string
    
    ApplicationRecord.connection.execute("
      ALTER TABLE strategy_selections
      ALTER COLUMN stack TYPE float USING NULLIF(stack, '')::float;
    ")
    add_index :strategy_selections, [:flops_key], name: "index_on_flops_key"
    add_index :strategy_selections, [:positions_key], name: "index_on_positions_key"
    
    StrategySelection.find_each do |strategy|
      flops_key = MySims::Index.new().convert_flops_key strategy[:flops]
      strategy.update(flops_key: flops_key, positions_key: strategy[:positions].join(''))
    end
  end

  def down
    remove_column :strategy_selections, :flops_key
    remove_column :strategy_selections, :positions_key
    change_column :strategy_selections, :stack, :string 
  end
end
