class AddDisplayPositionsByDefaultToUserPreferences < ActiveRecord::Migration[6.0]
  def change
    add_column :user_preferences, :display_positions_by_default, :boolean, default: true
  end
end
