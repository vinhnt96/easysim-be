class AddMoreColumnsForUsers < ActiveRecord::Migration[6.0]
  def change
    rename_column :users, :nickname, :username
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :birthday, :date
    add_column :users, :country, :string
  end
end
