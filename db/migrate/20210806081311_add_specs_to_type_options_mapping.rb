class AddSpecsToTypeOptionsMapping < ActiveRecord::Migration[6.1]
  def change
    add_column :type_options_mappings, :specs, :string
  end
end
