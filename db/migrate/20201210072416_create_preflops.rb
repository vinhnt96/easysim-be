class CreatePreflops < ActiveRecord::Migration[6.0]
  def up
    unless ApplicationRecord.connection.table_exists? :preflops
      create_table :preflops do |t|
        t.string :rake
        t.string :icm
        t.string :game_type
        t.string :stack_size
        t.string :node
        t.string :actions
        t.string :ranges
      end
    end
  end

  def down
    drop_table :preflops
  end
end

