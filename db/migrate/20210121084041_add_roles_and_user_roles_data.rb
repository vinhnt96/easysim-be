class AddRolesAndUserRolesData < ActiveRecord::Migration[6.0]
  def up
    ['admin', 'pokecode_user', 'odin_user'].each do |role_name| 
      Role.create(name: role_name)
    end

    User.all.each do |user|
      user.add_role :odin_user if user.roles.blank?
    end
  end

  def down
    User.all.each do |user|
      user.remove_role :odin_user if user.roles.present?
    end
    
    Role.all.delete_all
  end
end
