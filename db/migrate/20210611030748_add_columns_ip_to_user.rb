class AddColumnsIpToUser < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :current_ip_v4, :string
    add_column :users, :current_signin_ip_v4_at, :datetime
    add_column :users, :device, :string
    add_column :users, :browser, :string
    add_column :users, :online, :boolean
    add_column :users, :country_login, :string
  end

  def down
    remove_column :users, :current_ip_v4, :string
    remove_column :users, :current_signin_ip_v4_at, :datetime
    remove_column :users, :device, :string
    remove_column :users, :browser, :string
    remove_column :users, :online, :boolean, default: false
    remove_column :users, :country_login, :string
  end
end
