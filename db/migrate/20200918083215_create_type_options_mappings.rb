class CreateTypeOptionsMappings < ActiveRecord::Migration[6.0]
  def change
    create_table :type_options_mappings do |t|
      t.string :game_type
      t.string :stack
      t.string :positions, array: true, default: []
      t.string :type_options, array: true, default: []

      t.timestamps
    end
  end
end
