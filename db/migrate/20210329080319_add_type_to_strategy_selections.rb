class AddTypeToStrategySelections < ActiveRecord::Migration[6.0]
  def change
    add_column :strategy_selections, :strategy_selection_type, :string, default: :paid
  end
end
