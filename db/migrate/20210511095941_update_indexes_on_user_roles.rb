class UpdateIndexesOnUserRoles < ActiveRecord::Migration[6.0]
  def up
    remove_index :users_roles, name: :index_users_roles_on_user_id if index_exists?(:users_roles, :user_id, name: 'index_users_roles_on_user_id')
    remove_index :users_roles, name: :index_users_roles_on_role_id if index_exists?(:users_roles, :role_id, name: 'index_users_roles_on_role_id')
  end

  def down
    add_index :users_roles, :user_id, name: :index_users_roles_on_user_id unless index_exists?(:users_roles, :user_id, name: 'index_users_roles_on_user_id')
    add_index :users_roles, :role_id, name: :index_users_roles_on_role_id unless index_exists?(:users_roles, :role_id, name: 'index_users_roles_on_role_id')
  end
end
