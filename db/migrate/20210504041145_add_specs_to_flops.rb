class AddSpecsToFlops < ActiveRecord::Migration[6.0]
  def up
    unless column_exists? :flops, :specs
      add_column :flops, :specs, :string
    end
  end

  def down
    remove_column :flops, :specs
  end
end
