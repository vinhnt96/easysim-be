class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.references :plan
      t.references :user
      t.string :paypal_subscription_id
      t.datetime :start_time

      t.timestamps
    end
  end
end
