class CreateUserPreferences < ActiveRecord::Migration[6.0]
  def change
    create_table :user_preferences do |t|
      t.string :fold_color, default: 'red'
      t.string :check_and_call_color, default: 'green'
      t.string :bet_and_raise_color, default: 'purple'
      t.integer :user_id

      t.timestamps
    end
  end
end
