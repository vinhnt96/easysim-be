class FixPokerCodeUserRoleName < ActiveRecord::Migration[6.0]
  def up
    role = Role.find_by(name: 'pokecode_user')
    role.delete if role.present?

    Role.create(name: 'pokercode_user') unless Role.find_by(name: 'pokercode_user')
  end

  def down
    role = Role.find_by(name: 'pokercode_user')
    role.delete if role.present?

    Role.create(name: 'pokecode_user') unless Role.find_by(name: 'pokecode_user')
  end
end
