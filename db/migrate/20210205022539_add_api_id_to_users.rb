class AddApiIdToUsers < ActiveRecord::Migration[6.0]
  def up
    add_column :users, :api_id, :string, unique: true
    User.all.find_each do |user|
      user.update(api_id: SecureRandom.alphanumeric(6)) if user.api_id.nil?
    end
  end

  def down
    remove_columns :users, :api_id
  end
end
