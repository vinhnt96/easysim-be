class CreateStrategySelections < ActiveRecord::Migration[6.0]
  def change
    create_table :strategy_selections do |t|
      t.string :game_type
      t.string :stack
      t.string :positions, array: true, default: []
      t.string :sim_type
      t.string :flops, array: true, default: []

      t.timestamps
    end
  end
end
