class UpdateHideStrategyDefaultValueSecondTime < ActiveRecord::Migration[6.0]
  def up
    change_column :user_preferences, :hide_strategies_less_than, :float, default: 0.02
  end
  def down
    change_column :user_preferences, :hide_strategies_less_than, :float, default: 0.025
  end
end
