class ChangeDefaultHideStrategyToOnePercent < ActiveRecord::Migration[6.1]
  def up
    change_column :user_preferences, :hide_strategies_less_than, :float, default: 0.01
  end

  def down
    change_column :user_preferences, :hide_strategies_less_than, :float, default: 0.000
  end
end
