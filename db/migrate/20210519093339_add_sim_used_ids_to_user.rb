class AddSimUsedIdsToUser < ActiveRecord::Migration[6.0]
  def up
    unless column_exists?(:users, :sim_ids)
      add_column :users, :sim_ids, :integer, array: true, default: []
    end
  end

  def down
    if column_exists?(:users, :sim_ids)
      remove_column :users, :sim_ids, :integer, array: true, default: []
    end
  end
end
