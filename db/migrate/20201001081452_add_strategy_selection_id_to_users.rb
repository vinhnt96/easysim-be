class AddStrategySelectionIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :strategy_selection_id, :integer
  end
end
