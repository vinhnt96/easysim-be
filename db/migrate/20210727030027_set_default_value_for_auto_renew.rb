class SetDefaultValueForAutoRenew < ActiveRecord::Migration[6.1]
  def up
    change_column :subscriptions, :auto_renew, :boolean, default: true
  end

  def down
    change_column :subscriptions, :auto_renew, :boolean, default: nil
  end
end
