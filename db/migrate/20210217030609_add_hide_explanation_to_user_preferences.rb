class AddHideExplanationToUserPreferences < ActiveRecord::Migration[6.0]
  def change
    add_column :user_preferences, :hide_explanation, :boolean, default: false
  end
end
