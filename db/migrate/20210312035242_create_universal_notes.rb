class CreateUniversalNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :universal_notes do |t|
      t.text :note_data
      t.references :user

      t.timestamps
    end
    User.all.find_each do |user|
      UniversalNote.create(user_id: user[:id], note_data: { default_note: "", notes: []}.to_json) if user.universal_note.nil?
    end
  end
end
