class ContactUsMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :contact_us_messages do |t|
      t.string :name
      t.string :email
      t.string :message

      t.timestamps
    end
  end
end
