class AddColumnToSubscription < ActiveRecord::Migration[6.1]
  def up
    add_column :subscriptions, :code_discount, :string
    add_column :subscriptions, :status, :boolean, default: false
    remove_column :subscriptions, :paypal_subscription_id, :string
  end

  def down
    remove_column :subscriptions, :code_discount, :string
    remove_column :subscriptions, :status, :boolean, default: false
    add_column :subscriptions, :paypal_subscription_id, :string
  end
end
