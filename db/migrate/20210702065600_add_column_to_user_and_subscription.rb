class AddColumnToUserAndSubscription < ActiveRecord::Migration[6.1]
  def up
    add_column :subscriptions, :fastspring_subscription_id, :string
    add_column :subscriptions, :status, :integer, default: 0
  end

  def down
    remove_column :subscriptions, :fastspring_subscription_id
    remove_column :subscriptions, :status
  end
end
