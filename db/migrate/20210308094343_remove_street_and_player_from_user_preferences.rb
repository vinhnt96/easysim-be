class RemoveStreetAndPlayerFromUserPreferences < ActiveRecord::Migration[6.0]
  def up
    remove_columns :user_preferences, :simplifier_streets, :simplifier_player
  end
  def down
    add_column :user_preferences, :simplifier_streets, :integer, default: 1
    add_column :user_preferences, :simplifier_player, :string, array: true, default: []
  end
end
