class AddNumCombosOopAndNumCombosIpToFlops < ActiveRecord::Migration[6.0]
  def change
    add_column :flops, :num_combos_oop, :float
    add_column :flops, :num_combos_ip, :float
  end
end
