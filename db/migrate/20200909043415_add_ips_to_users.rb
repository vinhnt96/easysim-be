class AddIpsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :ips, :inet, array: true, default: []
  end
end
