class AddStrategyToFlops < ActiveRecord::Migration[6.0]
  def change
    add_column :flops, :strategy, :text
    rename_column :flops, :flops, :flop_cards
  end
end
