class CreateBugReports < ActiveRecord::Migration[6.0]
  def change
    create_table :bug_reports do |t|
      t.string :bug_type
      t.string :subject
      t.string :description
      t.string :page
      t.references :user

      t.timestamps
    end
  end
end
