class CreatePlans < ActiveRecord::Migration[6.0]
  def change
    create_table :plans do |t|
      t.string :paypal_product_id
      t.string :paypal_plan_id
      t.string :paypal_status
      t.string :name
      t.string :description
      t.float :price
      t.string :currency

      t.timestamps
    end
  end
end
