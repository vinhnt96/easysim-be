class AddBillingPeriodAndBillingFrequencyToPlans < ActiveRecord::Migration[6.0]
  def up
    add_column :plans, :billing_period, :string
    add_column :plans, :billing_frequency, :integer

    @paypal_api_access_token = GetPaypalApiAccessTokenService.call
    plans = Plan.all.find_each do |pl|
      plan = JSON.parse Admin::Paypal::PlanService.new(@paypal_api_access_token, {plan_id: pl[:paypal_plan_id]}).get_details
      pl.update(
        billing_period: plan['billing_cycles'][0]['frequency']['interval_unit'],
        billing_frequency: plan['billing_cycles'][0]['frequency']['interval_count']
      )
    end
  end

  def down
    remove_column :plans, :billing_period
    remove_column :plans, :billing_frequency
  end

end