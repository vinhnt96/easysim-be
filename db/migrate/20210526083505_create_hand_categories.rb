class CreateHandCategories < ActiveRecord::Migration[6.0]
  def up
    unless ApplicationRecord.connection.table_exists? :hand_categories
      create_table :hand_categories do |t|
        t.string :board 
        t.string :made_hands
        t.string :draw_hands
        t.timestamps
      end

      add_index :hand_categories, [:board], unique: true, name: "unique_board"
    end
  end

  def down
    # It empty instead of Drop table, because if using drop it will drop the table that customer has created and imported data
  end
end
