class AddActiveColumnToUserStrategySelections < ActiveRecord::Migration[6.1]
  def up
    add_column :user_strategy_selections, :active, :boolean, default: :true

    UserStrategySelection.includes(:strategy_selection).where(strategy_selection: {street: "preflop"}).update_all(active: false)
  end
  
  def down
    remove_column :user_strategy_selections, :active
  end
end
