class AddColumnsToUserStrategySelection < ActiveRecord::Migration[6.0]
  def change
    add_column :user_strategy_selections, :notes, :text
    add_column :user_strategy_selections, :date_last_view, :date
  end
end
