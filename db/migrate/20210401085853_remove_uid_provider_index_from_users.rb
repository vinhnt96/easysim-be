class RemoveUidProviderIndexFromUsers < ActiveRecord::Migration[6.0]
  def up
    remove_index :users, name: "index_users_on_uid_and_provider"
  end

  def down
    add_index :users, [:uid, :provider], unique: true
  end
end
