class AddFieldsToStrategySelections < ActiveRecord::Migration[6.0]
  def change
    add_column :strategy_selections, :street, :string, default: 'postflop'
    add_column :strategy_selections, :site, :string
    add_column :strategy_selections, :stake, :string
    add_column :strategy_selections, :open_size, :string
  end
end
