class AddSimplifierFieldsToUserPreferences < ActiveRecord::Migration[6.0]
  def change
    add_column :user_preferences, :simplifier_streets, :integer, default: 1
    add_column :user_preferences, :simplifier_player, :string, array: true, default: []
    add_column :user_preferences, :round_strategies_to_closest, :float, default: 0.01
    add_column :user_preferences, :hide_strategies_less_than, :float, default: 0.001
  end
end
