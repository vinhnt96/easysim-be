class UpdateIndexesForSomeTables < ActiveRecord::Migration[6.0]
  def up
    add_index :user_preferences, :user_id unless index_exists?(:user_preferences, :user_id)
    add_index :user_strategy_selections, [:user_id, :strategy_selection_id], name: 'index_on_user_id_and_strategy_selection_id' unless index_exists?(:user_preferences, [:user_id, :strategy_selection_id])
    add_index :users, :strategy_selection_id unless index_exists?(:users, :strategy_selection_id)
  end

  def down
    remove_index :user_preferences, :user_id
    remove_index :user_strategy_selections, [:user_id, :strategy_selection_id]
    remove_index :users, :strategy_selection_id
  end
end
