class AddIndexToPreflops < ActiveRecord::Migration[6.0]
  def change
    add_index :preflops, [:game_type, :stack_size, :node]
  end
end
