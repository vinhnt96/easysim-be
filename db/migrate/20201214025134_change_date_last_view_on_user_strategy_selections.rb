class ChangeDateLastViewOnUserStrategySelections < ActiveRecord::Migration[6.0]
  def change
    change_column :user_strategy_selections, :date_last_view, :datetime
  end
end
