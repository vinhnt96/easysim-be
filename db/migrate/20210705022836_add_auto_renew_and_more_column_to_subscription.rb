class AddAutoRenewAndMoreColumnToSubscription < ActiveRecord::Migration[6.1]
  def up
    add_column :subscriptions, :auto_renew, :boolean
    add_column :subscriptions, :cycle_time, :float
    add_column :subscriptions, :renew_time, :datetime
  end

  def down
    remove_column :subscriptions, :auto_renew
    remove_column :subscriptions, :cycle_time
    remove_column :subscriptions, :renew_time
  end
end
