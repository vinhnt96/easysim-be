class CreateTableActivities < ActiveRecord::Migration[6.1]
  def up
    create_table :activities do |t|
      t.datetime :logged_time
      t.datetime :signout_time
      t.datetime :begin_date
      t.integer :sim_ids, default: [], array: true
      t.timestamps
      t.index ["logged_time", "sim_ids"]
    end
    add_reference :activities, :user
  end

  def down
    remove_reference :activities, :user
    drop_table :activities
  end
end
