class AddFreeUserToRole < ActiveRecord::Migration[6.1]
  disable_ddl_transaction!
  def up
    add_column :users, :affiliate_code, :string
    Role.find_or_create_by(name: 'free_user') do |r|
      r.title = 'Free User'
    end
  end

  def down
    remove_column :users, :affiliate_code, :string
    role = Role.find_by(name: 'free_user')
    role.destroy if role.present?
  end
end
