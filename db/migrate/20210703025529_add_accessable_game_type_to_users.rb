class AddAccessableGameTypeToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :accessable_game_type, :string
  end
end
