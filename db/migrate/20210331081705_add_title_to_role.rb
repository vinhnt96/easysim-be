class AddTitleToRole < ActiveRecord::Migration[6.0]
  def up
    add_column :roles, :title, :string
    Role.all.find_each do |role|
      role.update(title: 'Admin') if role.name.casecmp? 'admin'
      role.update(title: 'Odin user') if role.name.casecmp? 'odin_user'
      role.update(title: 'PC design') if role.name.casecmp? 'pokercode_user'
    end
  end

  def down
    remove_columns :roles, :title, :string
  end
end
