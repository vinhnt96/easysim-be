class UpdateColumnsPreferences < ActiveRecord::Migration[6.0]
  def up
    change_column :user_preferences, :fold_color, :string, default: 'blue'
    change_column :user_preferences, :bet_and_raise_color, :string, default: 'red'
  end

  def down
    change_column :user_preferences, :fold_color, :string, default: 'red'
    change_column :user_preferences, :bet_and_raise_color, :string, default: 'purple'
  end
end
