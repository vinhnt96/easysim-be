class CreateStacksizes < ActiveRecord::Migration[6.0]
  def up
    unless ApplicationRecord.connection.table_exists? :stacksizes
      create_table :stacksizes do |t|
        t.string :filename
        t.float :initial_stacksize
      end
      add_index :stacksizes, [:filename], unique: true, name: "unique_filename"
    end
  end

  def down
    drop_table :preflops
  end
end
