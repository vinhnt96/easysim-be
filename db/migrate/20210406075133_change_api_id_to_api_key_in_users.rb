class ChangeApiIdToApiKeyInUsers < ActiveRecord::Migration[6.0]
  def change 
    rename_column :users, :api_id, :api_key
  end
end
