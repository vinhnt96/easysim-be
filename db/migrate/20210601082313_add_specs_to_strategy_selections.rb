class AddSpecsToStrategySelections < ActiveRecord::Migration[5.0]
  def up
    unless column_exists? :strategy_selections, :specs
      add_column :strategy_selections, :specs, :string

      StrategySelection.where(game_type: 'cash').update_all(specs: '25x6m500zs')
    end
  end

  def down
    remove_column :strategy_selections, :specs
  end
end
