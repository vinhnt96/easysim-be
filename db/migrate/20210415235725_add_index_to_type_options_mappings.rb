class AddIndexToTypeOptionsMappings < ActiveRecord::Migration[6.0]
  def change
    add_index :type_options_mappings, [:game_type, :stack, :positions], name: "index_on_game_type_and_stack_and_positions"
  end
end
