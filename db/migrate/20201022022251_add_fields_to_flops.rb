class AddFieldsToFlops < ActiveRecord::Migration[6.0]
  def change
    add_column :flops, :ranges_oop, :text
    add_column :flops, :ranges_ip, :text
    add_column :flops, :ev_oop, :text
    add_column :flops, :ev_ip, :text
    add_column :flops, :equity_oop, :text
    add_column :flops, :equity_ip, :text
  end
end
