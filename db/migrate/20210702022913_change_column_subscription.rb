class ChangeColumnSubscription < ActiveRecord::Migration[6.1]
  def up
    rename_column :subscriptions, :status, :paid
  end

  def down
    rename_column :subscriptions, :paid, :status
  end
end
