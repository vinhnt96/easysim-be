class CreateFlops < ActiveRecord::Migration[6.0]
  def change
    create_table :flops do |t|
      t.string :game_type
      t.string :stack_size
      t.string :sim_type
      t.string :flops
      t.string :positions
      t.string :nodes
      t.integer :node_count
      t.integer :pot_ip
      t.integer :pot_oop
      t.integer :pot_main

      t.timestamps
    end
  end
end
