class UpdateUserPreferencesForExistingUsers < ActiveRecord::Migration[6.0]
  def change
    User.all.each do |user|
      UserPreference.create(user_id: user.id)
    end
  end
end
