class AddCurrentSubscriptionToUsers < ActiveRecord::Migration[6.1]
  disable_ddl_transaction!
  def up
    add_column :users, :current_subscription_id, :integer
  end
  
  def down
    remove_column :users, :current_subscription_id
  end
end
