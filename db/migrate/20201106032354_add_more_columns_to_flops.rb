class AddMoreColumnsToFlops < ActiveRecord::Migration[6.0]
  def change
    add_column :flops, :probs_oop, :float
    add_column :flops, :probs_ip, :float
    add_column :flops, :probs, :float
    add_column :flops, :eq_oop2, :text
    add_column :flops, :ev_oop2, :text
    add_column :flops, :eq_ip2, :text
    add_column :flops, :ev_ip2, :text
    add_column :flops, :eq_node_oop, :float
    add_column :flops, :ev_node_oop, :float
    add_column :flops, :eq_node_ip, :float
    add_column :flops, :ev_node_ip, :float
    add_column :flops, :global_freq, :float
  end
end
