class ChangeColumnTablePlans < ActiveRecord::Migration[6.1]
  def up
    remove_column :plans, :paypal_plan_id, :string
    remove_column :plans, :paypal_status, :string
    rename_column :plans, :paypal_product_id, :product_id
    add_column :plans, :parent, :boolean, default: false
    add_column :plans, :parent_code, :string
  end

  def down
    add_column :plans, :paypal_plan_id, :string
    add_column :plans, :paypal_status, :string
    rename_column :plans, :product_id, :paypal_product_id
    remove_column :plans, :parent, :boolean, default: false
    remove_column :plans, :parent_code, :string
  end
end
