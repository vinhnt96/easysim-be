class AddIdToPreflops < ActiveRecord::Migration[6.0]
  def change
    unless column_exists?(:preflops, :id)
      add_column :preflops, :id, :primary_key
    end
  end
end
