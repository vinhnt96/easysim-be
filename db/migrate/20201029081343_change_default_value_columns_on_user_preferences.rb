class ChangeDefaultValueColumnsOnUserPreferences < ActiveRecord::Migration[6.0]
  def change
    change_column :user_preferences, :round_strategies_to_closest, :float, default: 0
    change_column :user_preferences, :hide_strategies_less_than, :float, default: 0.000
  end
end
